import db from "../firebase/firebase";
import dbClone from "../firebase/firebaseClone";
import modal from "modals";

export const newQuestion = (questionWithout) => {
    return () => {
        if (questionWithout.path !== '') {
            const userRefExisting = db.doc(questionWithout.path);
            const userRefExistingClone = dbClone.doc(questionWithout.path);
            console.log(questionWithout);
            userRefExisting.get()
                .then(doc => {
                    if (doc.exists) {
                        let question = {...questionWithout, path: questionWithout.path};
                        console.log(questionWithout.path);
                        userRefExisting.update({
                            question,
                            timestamp: new Date().getTime(),
                            createdAt: new Date(),
                        });
                        userRefExistingClone.update({
                            question,
                            timestamp: new Date().getTime(),
                            createdAt: new Date(),
                        })
                            .then(() => modal.alert({
                                message: "Question has been updated",
                                callback: function () {
                                    console.log('Question has been updated');
                                },
                                okTitle: 'OK'
                            }))
                    }
                })
                .catch(error => console.error("Error setting document: ", error));
        } else {
            const userRef = db.collection(`questionGroup/${questionWithout.questionGroup}/questions`);
            userRef.add({})
                .then(ref => {
                    let question = {...questionWithout, path: ref.path};
                    db.doc(ref.path).set({
                        question,
                        timestamp: new Date().getTime(),
                        createdAt: new Date(),
                    })
                        .then(() => {
                            console.log('New question has been added');
                        })
                        .catch(error => console.error("Error setting document: ", error));
                    dbClone.doc(ref.path).set({
                        question,
                        timestamp: new Date().getTime(),
                        createdAt: new Date(),
                    })
                        .then(() => modal.alert({
                            message: "New question has been added",
                            callback: () => {
                                console.log('New question has been added to the clone database');
                            },
                            okTitle: 'OK'
                        }))
                        .catch(error => console.error("Error setting document: ", error))
                });
        }
    };
};

export const pathExist = (path) => ({
    type: 'ADD_PATH',
    path
});


export const changeId = (id) => ({
    type: 'CHANGE_ID',
    id
});

export const startDeleteQuestion = (info) => {
    return (dispatch) => {
        if (info.path !== '') {
            db.doc(info.path).delete()
                .then(() => {
                    modal.alert({
                        message: "Question has been deleted",
                        callback: () => {
                            console.log("Question has been deleted")
                        },
                        okTitle: 'OK'
                    })
                })
                .catch(error => console.error("Error setting document: ", error));
            dbClone.doc(info.path).delete()
                .then(() => {
                    console.log('Question has been deleted from Clone database');
                })
                .catch(error => console.error("Error setting document: ", error));
            if (!info.main) {
                return dispatch(deleteQuestion(info.id));
            } else {
                return dispatch(deleteMainQuestion(info.id));
            }
        }
    }
};

export const deleteQuestion = (id) => ({
    type: 'DELETE_QUESTION',
    id
});


export const deleteMainQuestion = (id) => ({
    type: 'DELETE_MAIN_QUESTION',
    id
});

export const loadAdminQuestions = (allQuestions) => ({
    type: 'LOAD_ADMIN_QUESTIONS',
    allQuestions
});

export const loadAllDatabase = (questions) => ({
    type: 'LOAD_DATABASE_QUESTIONS',
    questions
});


export const questionId = (group) => {
    return async (dispatch) => {
        const snap = await db.collection(`questionGroup/${group.questionGroup}/questions`).get();
        let size = snap.size;
        dispatch(changeId(size + 1))
    }
};


export const graphUrl = (graphUrl) => ({
    type: 'GRAPH_URL',
    graphUrl
});

export const graphName = (graph) => ({
    type: 'GRAPH_NAME',
    graph
});

export const mainDatabaseBoolean = () => ({
    type: 'MAIN_BOOLEAN',
});

export const graphMetadata = (metadata) => ({
    type: 'GRAPH_METADATA',
    metadata
});


export const correctPassword = (text) => ({
    type: 'CORRECT_PASSWORD',
    text
});

export const selectGraphsList = (allMetadata) => ({
    type: 'SELECT_GRAPH_LIST',
    allMetadata
});




