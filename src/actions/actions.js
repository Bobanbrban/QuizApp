import db from '../firebase/firebase';
import dbClone from "../firebase/firebaseClone";
import moment from 'moment';

export const nextQuestion = () => ({
    type: 'NEXT_QUESTION',
});


export const countDownSec = () => ({
    type: 'COUNTDOWN_SEC',
});

export const toogleInterval = (enabled) => ({
    type: 'INTERVAL',
    enabled
});

export const selectedAnswerText = ({answer}) => ({
    type: 'SELECTED_ANSWER',
    answer
});

export const allAnswers = ({answer}) => ({
    type: 'ANSWERS_UPDATE',
    answer
});

export const allAnswersResults = (answers) => ({
    type: 'ALL_ANSWERS',
    answers
});



export const loadAnswers  = (answers) => ({
    type: 'LOAD_ANSWERS',
    answers
});

export const loadAnswersDone  = (answers) => ({
    type: 'LOAD_ANSWERS_DONE',
    answers
});

export const startLoadAnswersDone  = (answers) => {
    return (dispatch) => {
        return dispatch(loadAnswersDone(answers));
    }
};

export const startAllAnswers = (allAnswersInfo) => {
    return () => {
        const {answers, user} = allAnswersInfo;
        let answersParsed = JSON.parse(JSON.stringify(answers));
        const userRef = db.doc(`users/${allAnswersInfo.curp}`);
        const userRefClone = dbClone.doc(`users/${allAnswersInfo.curp}`);
        let createdAt = moment().format('MMMM Do YYYY');
        let total = JSON.parse(localStorage.getItem("total"))
        let infoResults = {user, total : total, createdAt};
        const userRefTotal = db.doc(`totalMail/${allAnswersInfo.curp}`);
        userRefTotal.set(infoResults);
        userRefTotal.delete();
        userRef.update({
            allAnswers: answersParsed,
            total: total,
            isHidden: true,
            completed: true,
            started: true,
            secondStepStarted: false,
            secondStepCompleted: false,
            secondStepResult: 0,
            secondStepTime: "0:00:00",
            createdAt: moment().format('MMMM Do YYYY'),
            timestamp: new Date().getTime(),
            riddleObject: {
                riddleMin: 0,
                riddleSec: 0,
                riddleHour: 0,
                riddleResult: 0
            },
            letquestion2answer: {
                question2min: 0,
                question2hour: 0,
                question2sec: 0,
            },
        })
            .then(() => console.log("data was set to the database"))
            .catch(error => console.error("Error adding document: ", error));

        userRefClone.update({
            allAnswers: answersParsed,
            total: total,
            isHidden: false,
            completed: true,
            started: true,
            secondStepStarted: false,
            secondStepCompleted: false,
            secondStepResult: 0,
            secondStepTime: "0:00",
            createdAt,
            timestamp: new Date().getTime(),
            riddleObject: {
                riddleMin: 4,
                riddleSec: 4,
                riddleHour: 4,
                riddleResult: 4
            },
            letquestion2answer: {
                question2min: 4,
                question2hour: 4,
                question2sec: 4,
            },
        })
            .then(() => console.log("data was set to the database"))
            .catch(error => console.error("Error adding document: ", error));
    }
};

export const totalPoints = ({total}) => ({
    type: 'TOTAL_POINTS',
    total
});

export const totalPossible = ({possible}) => ({
    type: 'TOTAL_POSSIBLE',
    possible
});


export const loadQuestions = (allQuestions) => ({
    type: 'LOAD_QUESTIONS',
    allQuestions,
});


export const addCompleted = () => ({
    type: 'ADD_COMPLETED',
});


export const addUserInfo = (userInfo = {}) => ({
    type: 'ADD_USER_INFO',
    userInfo
});

export const addCurp = (curp) => ({
    type: 'ADD_CURP',
    curp
});


export const clearLocalStorage = () => ({
    type: 'CLEAR_STORAGE',
});

export const clearOnStart = ({min,total}) => ({
    type: 'CLEAR_START',
    min,
    total
});



