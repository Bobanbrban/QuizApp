export const loginPage = () => ({
    type: 'LOGIN_PAGE',
});

export const progressPageAction = () => ({
    type: 'PROGRESS_PAGE',
});

export const questionsPage = () => ({
    type: 'QUESTIONS_PAGE',
});

export const secondStagePage = () => ({
    type: 'SECONDSTEP_PAGE',
});

export const resultsPageAction = () => ({
    type: 'RESULTS_PAGE',
});
