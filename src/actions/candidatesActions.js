export const loadCandidates = (candidatesData) => ({
    type: 'LOAD_CANDIDATES',
    candidatesData
});

export const filteredCandidates = (filteredData) => ({
    type: 'FILTER_CANDIDATES',
    filteredData
});

export const openDiv = (index) => ({
    type: 'OPEN_DIV',
    index
});

export const addCommentLocally = (info) => ({
    type: 'ADD_COMMENT_LOCALLY',
    index: info.index,
    comment: info.thisComment
});

export const invitedForInterview = (indexObj) => ({
    type: 'INTERVIEW',
    index: indexObj.index,
    interviewDateDataBase: indexObj.interviewDateDataBase,
    interviewTimestamp: indexObj.interviewTimestamp
});