export const riddleAction = () => ({
    type: 'RIDDLE_SELECTED',
});

export const question2Action = () => ({
    type: 'QUESTION2_SELECTED',
});

export const question3Action = () => ({
    type: 'QUESTION3_SELECTED',
});

export const finalPageAction = () => ({
    type: 'FINAL_PAGE',
});

export const secondStepStarted = () => ({
    type: 'SECOND_STARTED',
});

export const thankYouPage = () => ({
    type: 'THANK_YOU',
});

export const backToResults = () => ({
    type: 'BACK_RESULTS',
});

export const submitInfo = () => ({
    type: 'CANDIDATE_INFO',
});

export const quizFailed = () => ({
    type: 'QUIZ_FAILED',
});

export const progresPageAction = () => ({
    type: 'PROGRESS_PAGE',
});


export const secondStepQualified = () => ({
    type: 'FOR_SECOND',
});


export const secondStepCompleted = () => ({
    type: 'SECOND_COMPLETED',
});


export const qualifiedForInterview = () => ({
    type: 'FOR_INTERVIEW',
});

export const qualifiedForTraining = () => ({
    type: 'FOR_TRAINING',
});

