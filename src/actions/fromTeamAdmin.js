import db from "../firebase/firebase";
import dbClone from "../firebase/firebaseClone";
import modal from "modals";

export const newQuestionTeam = (questionWithout) => {
    return () => {
        const userRef = db.collection(`team`);
        userRef.add({})
            .then(ref => {
                let question = {...questionWithout, path: ref.path};
                db.doc(ref.path).set({
                    question,
                    timestamp: new Date().getTime(),
                    createdAt: new Date(),
                });
                dbClone.doc(ref.path).set({
                    question,
                    timestamp: new Date().getTime(),
                    createdAt: new Date(),
                });
                modal.alert({
                    message: "Question has been added",
                    callback: function () {
                        console.log('Question has been added');
                        document.querySelector('.questionDiv').reset();
                    },
                    okTitle: 'OK'
                })
            })
            .catch(error => console.error("Error adding document: ", error));
    };
};

