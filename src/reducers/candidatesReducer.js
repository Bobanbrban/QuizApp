let defaultState = {
    onLoadState: [],
    filteredState: []
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'LOAD_CANDIDATES':
            let loadNewState = [...action.candidatesData];
            return {
                ...state,
                onLoadState: loadNewState,
                filteredState: loadNewState
            };
        case 'FILTER_CANDIDATES':
            const newOnLoadStateFilter = [...action.filteredData];
            newOnLoadStateFilter.map(cand => cand.isHidden = true);
            return {
                ...state,
                filteredState: newOnLoadStateFilter
            };
        case 'OPEN_DIV':
            const newOnLoadStateOpen = [...state.filteredState];
            newOnLoadStateOpen[action.index].isHidden = !newOnLoadStateOpen[action.index].isHidden;
            return {
                ...state,
                filteredState: newOnLoadStateOpen,
            };
        case 'ADD_COMMENT_LOCALLY':
            const newOnLoadState = [...state.filteredState];
            newOnLoadState[action.index].ourComment = action.comment;
            const newFilterStateAdd = [...state.filteredState];
            newFilterStateAdd[action.index].ourComment = action.comment;
            return {
                ...state,
                onLoadState: newOnLoadState,
                filteredState: newFilterStateAdd
            };
        case 'INTERVIEW':
            console.log(action);
            const newStateInterview = [...state.filteredState];
            newStateInterview[action.index].invitedForInterview = true;
            newStateInterview[action.index].interviewDate = action.interviewDateDataBase;
            newStateInterview[action.index].interviewTimestamp = action.interviewTimestamp;
            const newOnLoadStateInterview = [...state.filteredState];
            newOnLoadStateInterview[action.index].invitedForInterview = true;
            newOnLoadStateInterview[action.index].interviewDate = action.interviewDateDataBase;
            newStateInterview[action.index].interviewTimestamp = action.interviewTimestamp;
            return {
                ...state,
                onLoadState: newOnLoadStateInterview,
                filteredState: newStateInterview
            };
        default:
            return state;
    }
};
