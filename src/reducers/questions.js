// noinspection JSAnnotator

export default (state = [], action) => {
    switch (action.type) {
        case 'SELECTED_ANSWER':
            return state;
        case 'LOAD_QUESTIONS':
            return [
                ...state,
                ...action.allQuestions
            ];
        default:
            return state;
    }
};
