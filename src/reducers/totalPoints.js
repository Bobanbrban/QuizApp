
export default (state = {
    totalPoints: 0,
    totalPossible: 0
}, action) => {
    switch (action.type) {
        case 'TOTAL_POINTS':
            localStorage.setItem("total", state.totalPoints + action.total);
        return {
            ...state,
            totalPoints: state.totalPoints + action.total
        };
        case 'TOTAL_POSSIBLE':
            localStorage.setItem("totalPossible", state.totalPossible + action.possible);
            return {
                ...state,
                totalPossible: state.totalPossible + action.possible
            };
        case 'CLEAR_STORAGE':
            return {
                ...state,
                totalPoints: 0,
                totalPossible: 0
            };
        default:
            return state;
    }
};