export default (state = {
    quiz: false,
    forSecond: false,
    completedSecond: false,
    qualifiedInterview: false,
    qualifiedTraining: false,
}, action) => {
    switch (action.type) {
        case  'QUIZ_FAILED':
            return {
                ...state,
                quiz: true,
                secondStepQualified: false,
                secondStepCompleted: false,
                qualifiedInterview: false,
                qualifiedTraining: false,
            };
        case  'FOR_SECOND':
            return {
                ...state,
                quiz: false,
                secondStepQualified: false,
                secondStepCompleted: false,
                qualifiedInterview: false,
                qualifiedTraining: false,
            };
        case  'SECOND_COMPLETED':
            return {
                ...state,
                quiz: false,
                secondStepQualified: true,
                secondStepCompleted: true,
                qualifiedInterview: false,
                qualifiedTraining: false,
            };
        case  'FOR_INTERVIEW':
            return {
                ...state,
                quiz: false,
                secondStepQualified: false,
                secondStepCompleted: false,
                qualifiedInterview: true,
                qualifiedTraining: false,
            };
        case  'FOR_TRAINING':
            return {
                ...state,
                quiz: false,
                secondStepQualified: false,
                secondStepCompleted: false,
                qualifiedInterview: false,
                qualifiedTraining: true,
            };
        default:
            return state;
    }
};
