// noinspection JSAnnotator

let storedAnswers = JSON.parse(localStorage.getItem('answers'));

const defaultState = storedAnswers || [];

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ANSWERS_UPDATE':
            let allAnswers = [...state, action.answer];
            localStorage.setItem("answers", JSON.stringify(allAnswers));
            return [
                ...state,
              action.answer
            ];
        case 'LOAD_ANSWERS':
            return [
                ...state=[],
                ...action.answers
                ];
        case 'LOAD_ANSWERS_DONE':
            return [
                ...state=[],
                ...action.answers
            ];
        case 'ALL_ANSWERS':
            console.log(action);
            return [
                ...state=[],
                ...action.answers
            ];
        case 'CLEAR_STORAGE':
            return [
                ...state=[]
            ];
        default:
            return state;
    }
};