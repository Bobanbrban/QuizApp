// noinspection JSAnnotator
import db from "../firebase/firebase";


let minDb = 60;
let docRef = db.doc('databaseSet/settings');
docRef.get()
    .then(async data => {
        minDb = await data.data().min;
    })
    .catch(err => console.error(err));

let storedOrder = JSON.parse(localStorage.getItem("order")) || 0;
let storedQuestionTotal = JSON.parse(localStorage.getItem("totalQuestions")) || 25;
let parsedMin = JSON.parse(localStorage.getItem("min")) < minDb ? JSON.parse(localStorage.getItem("min")) : minDb;
let storedMin =  parsedMin === null ? minDb : parsedMin;
let storedSec = JSON.parse(localStorage.getItem("sec")) || 0;
let storedUserInfo = JSON.parse(localStorage.getItem("userInfo")) || {};
let storedCompleted = JSON.parse(localStorage.getItem("completed")) || false ;
let startedStored = JSON.parse(localStorage.getItem("started")) || false ;
let storedCurp = typeof localStorage.getItem("curp") === "string" && localStorage.getItem("curp") ? JSON.parse(localStorage.getItem("curp")) :  '';
let storedThankYou = JSON.parse(localStorage.getItem("thankYou")) || false;
let progressBarLoaded = JSON.parse(localStorage.getItem("progressBarLoaded")) || false;
const defaultState = {
    order: storedOrder,
    min: storedMin,
    sec: storedSec,
    timeout: 1000,
    enabled: false,
    totalQuestions: storedQuestionTotal,
    userInfo: storedUserInfo,
    completed: storedCompleted,
    started: startedStored,
    curp: storedCurp,
    thankYou: storedThankYou,
    progressBarLoaded: progressBarLoaded
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'NEXT_QUESTION':
            let orderNumber = state.order + 1;
            localStorage.setItem("order" , JSON.stringify(orderNumber));
            return {
                ...state,
                order: orderNumber
            };
        case 'COUNTDOWN_SEC':
            let secDisplayed;
            if (state.sec < 1 && state.min !== 0) {
                secDisplayed = 59;
            } else if (state.sec === 0 && state.min === 0 ) {
                secDisplayed = 0;
            } else {
                secDisplayed = state.sec - 1
            }
            let min;
            if (state.sec < 1 && state.min !== 0) {
                min = state.min - 1;
            } else if (state.sec === 0 && state.min === 0 ) {
                min = 0;
            } else {
                min = state.min
            }
            localStorage.setItem("min" , JSON.stringify(min));
            localStorage.setItem("sec" , JSON.stringify(secDisplayed));
            return {
                ...state,
                sec: secDisplayed,
                min: min
            };
        case 'ADD_USER_INFO':
            localStorage.setItem("userInfo" , JSON.stringify(action.userInfo));
            return {
                ...state,
                userInfo: action.userInfo
            };
        case 'ADD_CURP':
            localStorage.setItem("curp" , JSON.stringify(action.curp));
            console.log(action.curp);
            return {
                ...state,
                curp: action.curp,
            };
        case 'ADD_COMPLETED':
           localStorage.setItem("completed" , JSON.stringify(true));
            return {
                ...state,
                completed: true,
            };
        case 'INTERVAL':
            return {
                ...state,
                enabled: action.enabled,
            };
        case 'CLEAR_STORAGE':
            localStorage.setItem('started', JSON.stringify(true));
            localStorage.removeItem('answers');
            localStorage.removeItem('questions');
            localStorage.removeItem('userInfo');
            localStorage.removeItem('order');
            localStorage.removeItem('completed');
            localStorage.removeItem('progressBarLoaded');
            return {
                ...state,
                started: true,
                completed: false,
                progressBarLoaded: false
            };
        case 'CLEAR_START':
            localStorage.removeItem('answers');
            localStorage.removeItem('questions');
            localStorage.removeItem('min');
            localStorage.removeItem('sec');
            localStorage.removeItem('userInfo');
            localStorage.removeItem('order');
            localStorage.removeItem('totalQuestions');
            localStorage.setItem("min" , JSON.stringify(minDb));
            localStorage.setItem("totalQuestions" , JSON.stringify(action.total));
            return {
                ...state,
                sec: 0,
                min: minDb,
                order: 0,
                totalQuestions: action.total
            };
        case 'THANK_YOU':
            return {
                ...state,
                thankYou: true
            };
        case 'BACK_RESULTS':
            return {
                ...state,
                thankYou: false,
            };
        default:
            return state;
    }
};