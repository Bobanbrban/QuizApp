
let progress = JSON.parse(localStorage.getItem("progressBarLoaded")) || false;
let loading = progress === true ? false : true;

export default (state = {
    loadingCube: loading,
    logInPage: false,
    progressPage: progress,
    questions: false,
    secondStep: false,
    resultsPage: false
}, action) => {
    switch (action.type) {
        case  'LOGIN_PAGE':
            return {
                ...state,
                loadingCube: false,
                logInPage: true,
                progressPage: false,
                questions: false,
                secondStep: false,
                resultsPage: false
            };
        case  'PROGRESS_PAGE':
            return {
                ...state,
                loadingCube: false,
                logInPage: false,
                progressPage: true,
                questions: false,
                secondStep: false,
                resultsPage: false
            };

        case 'QUESTIONS_PAGE':
            return {
                ...state,
                loadingCube: false,
                logInPage: false,
                progressPage: false,
                questions: true,
                secondStep: false,
                resultsPage: false
            };
        case 'SECONDSTEP_PAGE':
            return {
                ...state,
                loadingCube: false,
                logInPage: false,
                progressPage: false,
                questions: false,
                secondStep: true,
                resultsPage: false
            };
        case 'RESULTS_PAGE':
            return {
                ...state,
                loadingCube: false,
                logInPage: false,
                progressPage: false,
                questions: false,
                secondStep: false,
                resultsPage: true
            };

        default:
            return state;
    }
};
