let defaultState = [
    {text: "Welcome to the Agave Lab software development aptitude Quiz"},
    {text: "Quiz has 20 questions and time limit is 20 minutes to answer them"},
    {text:"Each correct answer can bring you 4 or 8 points with the maximum total of 100 points"},
    {text: "Each candidate can have one attempt within 2 months"},
    {text: "If you have done quiz already, after pressing the start button, you will be redirected to the results page"},
    {text: "Who scores 60 or more points, qualifies for the second stage with 3 additional questions"},
    {text: "If you qualify for the second stage, after 10 seconds on result page you will be redirected to additional questions"},
    {text: "Make sure that you have active google account which you will be using during the entire process"},
    {text: "To begin with the quiz, please type your valid curp number and press START button. Good luck !"}
];

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'INFO_TEXT':
            return state;
        default:
            return state;
    }
};
