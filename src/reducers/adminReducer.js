let storedPassword = JSON.parse(localStorage.getItem("password")) || false;
let storedAdminOrTeam = JSON.parse(localStorage.getItem("adminOrTeam")) || '';


export default (state = {
    adminPassword: storedPassword ,
    adminId: 0,
    adminTeam: 0,
    graphUrl: 'https://firebasestorage.googleapis.com/v0/b/quizapp-d71a8.appspot.com/o/agaveTeam%2F1530650205899Agave_lab.png?alt=media&token=a378f70d-738f-4f77-a1f0-aebcb524c7f3',
    questions: [],
    questionsDatabase: [],
    mainDatabase: true,
    graph: "",
    graphMetadata: {},
    path: '',
    allMetadata: [],
    adminOrTeam: storedAdminOrTeam,
}, action) => {
    switch (action.type) {
        case 'CHANGE_ID':
            let int = parseInt(action.id, 10);
            return {
                ...state,
                adminId: int
            };
        case 'CHANGE_ID_TEAM':
            let intTeam = parseInt(action.id, 10);
            return {
                ...state,
                adminTeam: intTeam
            };
        case 'GRAPH_URL':
            return {
                ...state,
                graphUrl: action.graphUrl
            };
        case 'GRAPH_NAME':
            return {
                ...state,
                graph: action.graph
            };
        case 'LOAD_ADMIN_QUESTIONS':
            return {
                ...state,
                questions: action.allQuestions
            };
        case "LOAD_DATABASE_QUESTIONS":
            return {
                ...state,
                questionsDatabase: action.questions
            };
        case 'DELETE_QUESTION':
            return {
                ...state,
                questions: [
                    ...state.questions.filter((data,i) => i !== parseInt(action.id, 10))
                ],
            };
        case 'DELETE_MAIN_QUESTION':
            return {
                ...state,
                questionsDatabase: [
                    ...state.questionsDatabase.filter((data,i) => i !== parseInt(action.id, 10))
                ],
            };
        case 'MAIN_BOOLEAN':
            return {
                ...state,
                mainDatabase: !state.mainDatabase
            };
        case 'GRAPH_METADATA':
            return {
                ...state,
                graphMetadata: action.metadata
            };
        case 'ADD_PATH':
            return {
                ...state,
                path: action.path
            };
        case 'CORRECT_PASSWORD':
            localStorage.setItem("password" , true);
            localStorage.setItem("adminOrTeam" , JSON.stringify(action.text));
            return {
                ...state,
                adminPassword: true,
                adminOrTeam:action.text
            };
        case 'SELECT_GRAPH_LIST':
            return {
                ...state,
                allMetadata: action.allMetadata
            };
        default:
            return state;
    }
};