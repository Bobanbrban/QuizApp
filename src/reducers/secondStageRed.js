let secondStepStarted = JSON.parse(localStorage.getItem("secondStep")) || false;
let finalePage = JSON.parse(localStorage.getItem("finalePage")) || false;
let progressPage = JSON.parse(localStorage.getItem("progressPage")) || false;

export default (state = {
    candidateInfo: false,
    progressPage: progressPage,
    riddle: false,
    question2: false,
    question3: false,
    secondStep: secondStepStarted,
    finalPage: finalePage,
}, action) => {
    switch (action.type) {
        case  'CANDIDATE_INFO':
            return {
                ...state,
                candidateInfo: true,
            };
        case  'THIRD_STEP':
            return {
                ...state,
                thirdStepTrue: true,
            };
        case 'RIDDLE_SELECTED':
            return {
                ...state,
                riddle: true,
                question2: false,
                question3: false,
                candidateInfo: false,
            };
        case 'QUESTION2_SELECTED':
            return {
                ...state,
                question2: true,
                question3: false,
                riddle: false
            };
        case 'QUESTION3_SELECTED':
            return {
                ...state,
                question3: true,
                question2: false,
                riddle: false
            };
        case 'FINAL_PAGE':
            return {
                ...state,
                question3: false,
                question2: false,
                riddle: false,
                finalPage: true
            };
        case  'SECOND_STARTED':
            return {
                ...state,
                secondStep: true,
            };
        default:
            return state;
    }
};
