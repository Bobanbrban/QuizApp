import   firebase from 'firebase/app';
import 'firebase/storage'
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/auth';

let configClone = {
    apiKey: "AIzaSyD2IMI9i9Gw6mskN9PlCNbmk7vDMzGpNYA",
    authDomain: "quizapp-d71a8.firebaseapp.com",
    databaseURL: "https://quizapp-d71a8.firebaseio.com",
    projectId: "quizapp-d71a8",
    storageBucket: "quizapp-d71a8.appspot.com",
};


let secondary = firebase.initializeApp(configClone, "secondary");

const settings = {/* your settings... */ timestampsInSnapshots: true};
const dbClone = secondary.firestore();
dbClone.settings(settings);

export const storageRefClone = secondary.storage();


export { secondary };
export default dbClone;
