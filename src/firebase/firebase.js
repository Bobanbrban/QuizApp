import  firebase from 'firebase/app';
import 'firebase/storage'
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/auth';


// let config = {
//     apiKey: "AIzaSyAbm5_aNobIuDMQJLFjyQUSPwJc3dYL-1s",
//     authDomain: "agavequiz.firebaseapp.com",
//     databaseURL: "https://agavequiz.firebaseio.com",
//     projectId: "agavequiz",
//     storageBucket: "agavequiz.appspot.com",
// };

let config = {
    apiKey: "AIzaSyAy86vd-Lp-WqHhIRuScDLs0SsdyFsFQ-k",
    authDomain: "agavequiz-main.firebaseapp.com",
    databaseURL: "https://agavequiz-main.firebaseio.com",
    projectId: "agavequiz-main",
    storageBucket: "agavequiz-main.appspot.com",
    messagingSenderId: "1083110456651"
};

firebase.initializeApp(config);

const settings = {/* your settings... */ timestampsInSnapshots: true};
const db = firebase.firestore();
db.settings(settings);

export const storageRef = firebase.storage();

let provider = new firebase.auth.GoogleAuthProvider();
provider.addScope('profile');
provider.addScope('email');


export { firebase, provider };
export default db;
