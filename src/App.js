import React from 'react';
import Quiz from './components/Quiz';
import { Provider } from 'react-redux';
import configureStore from './components/store';
import './stylesheets/style.css';

const store = configureStore();

const App = () =>  (
    <Provider store={store}>
        <Quiz />
    </Provider>
);

export default App;

