import React from "react";

class AdminOnly extends React.Component  {
    render() {
        return (
                    <div className="adminLogin">
                        <form className="passwordForm" onSubmit={this.props.adminPassword}>
                            <input className="adminPasswordInput" placeholder='password' required/>
                            <button type="button" className="passwordButton" onClick={this.props.adminPassword}>submit</button>
                        </form>
                    </div>
        )
    }
}

export default AdminOnly;

