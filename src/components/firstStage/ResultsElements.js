import React from 'react';

class ResultsElemnts extends React.Component {

    render() {
          let answer = this.props.answerLi;
          let correct = answer.answer === answer.correctAnswer ? "Correct" : "Incorrect";

        return (
           <li>
               <span>{answer.qustionOrderNumber}</span>
               <span>{correct}</span>
               <span className="pointsSpan">points: {this.props.result}</span>
           </li>
        )
    }
}


export default ResultsElemnts;

