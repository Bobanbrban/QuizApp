import React from 'react';
import { connect } from 'react-redux';
import Results from "./results";
import { startAllAnswers, startLoadAnswersDone, clearLocalStorage } from "../../actions/actions";
import { progressPageAction } from "../../actions/pagesActions";
import {  secondStepStarted } from "../../actions/secondStageActions"

class ResultsPage extends React.Component {

    componentDidMount() {
        let updateResults = () => {
            let answers = this.props.answers;
            let totalObject = {total: this.props.total};
            let curp = this.props.curp.toUpperCase();
            let user = this.props.user;
            const allAnswersInfo = {answers: answers , totalObject: totalObject, curp: curp, user: user};
            this.props.startAllAnswers(allAnswersInfo);
        };
        setTimeout(updateResults(),0);
        let pushToProgress = () => {
            this.props.progressPageAction();
        };
        setTimeout(() => pushToProgress(),5000);
    };

    pushToProgress = () => {
        this.props.progressPageAction();
    };


    render() {
        let possible = this.props.totalPossible;
        let total = this.props.total;
        let subtracted = possible - total;
        let sum = possible + total;
        return (
            <div className="resultsPage">
                <Results answers={this.props.answers}/>
                <section className="nextStageDiv">
                    <div id="resultTotal">Total<br/>{this.props.total}/{this.props.totalPossible}</div>
                    <div className="secondStepButton">
                        <div className='congratulationsTop'>{sum !== 0 && subtracted < 41 ? 'Congratulations!!!' : `Sorry, your score is below our minimum threshold of 60 points`}</div>
                        <div className='congratulationsBottom'>{sum !== 0 && subtracted < 41 ? "You have qualified for second stage" : ` `}</div>
                        <button onClick={this.pushToProgress} className="moreInfoSecondStage">More info</button>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStatetoProps = (state) => ({
    answers: state.answers,
    total: state.total.totalPoints,
    user: state.order.userInfo,
    curp: state.order.curp,
    totalPossible: state.total.totalPossible
});

const mapDispatchToProps = (dispatch) => ({
    startAllAnswers: (allAnswersInfo) => dispatch(startAllAnswers(allAnswersInfo)),
    startLoadAnswersDone: (answers) => dispatch(startLoadAnswersDone(answers)),
    clearLocalStorage: () => dispatch(clearLocalStorage()),
    secondStepStarted: () => dispatch(secondStepStarted()),
    progressPageAction: () => dispatch(progressPageAction())
});

export default connect(mapStatetoProps, mapDispatchToProps)(ResultsPage);