import React from 'react';
import { connect } from 'react-redux';
import AnswerOptions from "./answerOptions";


class Questions extends React.Component {
    render() {
        const {image,questionGraph} = this.props.info;
        let text = this.props.text;
        let options = this.props.options;
        return (
            <div className="questionAndOptionsDiv">
                <div className="questionTitle">
                    <div className="graphQuestion">
                        <img className="graphImage" src={image} alt=""/>
                    </div>
                    <div className="fullQuestion">
                        {questionGraph ? text : ''}
                    </div>
                </div>
                <AnswerOptions options={questionGraph ? options : []}
                               image={image}/>
            </div>
        )
    }
};

const mapStatetoProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});


export default connect( mapStatetoProps, mapDispatchToProps )(Questions);
