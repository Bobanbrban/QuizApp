import React from 'react';
import db, {firebase,provider} from "../../firebase/firebase";
import dbClone from "../../firebase/firebaseClone";
import modal from "modals";
import {
    addCurp,
    addCompleted,
    nextQuestion,
    countDownSec,
    toogleInterval,
    selectedAnswerText,
    allAnswers,
    clearLocalStorage,
    addUserInfo,
    loadAnswers,
    startAllAnswers,
    clearOnStart,
    loadQuestions
} from "../../actions/actions";
import {
    loginPage,
    progressPageAction,
    questionsPage,
    resultsPageAction,
    secondStagePage
} from "../../actions/pagesActions";
import {startLogin, startLoginRedirect} from "../../actions/auth";


import connect from "react-redux/es/connect/connect";

class LoginInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            cities: [],
            opacity: 1,
            city: '',
            doneAlready: false,
            smallerScreen: this.props.smallerScreen,
            loginUi: false,
            display: "none",
        }
    }

    componentDidMount() {
        let changeSelectColor = () => {
            document.querySelector(".candidateInfoAge").classList.add("selectColor");
            document.querySelector(".candidateInfoDirection").classList.add("selectColor");
        };
        changeSelectColor();

        let ageOptions = () => {
            let i;
            for (i = 21; i < 65; i++) {
                let option = document.createElement("option");
                option.setAttribute('data-index', `${i}`);
                option.setAttribute('id', `${i}`);
                option.setAttribute('value', `${i}`);
                option.innerHTML = `${i}`;
                let selectAge = document.querySelector('.candidateInfoAge');
                selectAge.appendChild(option);
            };
        };
        ageOptions();

        let getMexicanCities = async () => {
            let response = await fetch("https://raw.githubusercontent.com/slobodanboba/world-map/master/src/content/city_list.json");
            let data = await response.json();
            let mexicanCities = data.filter(city => city.country === "MX");
            this.setState({
                cities: mexicanCities
            });
        };
        getMexicanCities();
    }



    findMatches = (wordToMatch, names) => {
        return names.filter(city => {
            // here we need to figure out if the city or state matches what was searched
            let wordToMatchSmall = wordToMatch.toLowerCase();
            return city.toLowerCase().startsWith(wordToMatchSmall);
        });
    };

    displayMatches = (e) => {
        let searchInput = document.querySelector('.candidateInfoCity').value;
        if (searchInput === "") {
            this.setState({
                opacity: 1
            })
        } else {
            this.setState({
                opacity: 0
            })
        }
        if (searchInput.slice(-1) !== "\\") {
            let cities = [];
            let mexicanCities = this.state.cities.filter(city => city.country === "MX").sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));
            mexicanCities.map(city => cities.push(city.name));
            let matched = this.findMatches(searchInput, cities);
            let searchInputElement = document.querySelector('.candidateInfoCitySpan');
            let searchInputCapital = typeof searchInput[0] !== "undefined" ? searchInput[0].toUpperCase() + searchInput.substring(1) : '';
            if (matched.length === 0 && searchInput !== "") {
                let bolded = `<span class="autoComplete">${searchInputCapital}</span>`;
                searchInputElement.innerHTML = bolded;
                this.setState({
                    city: searchInput
                });
            } else {
                const regex = new RegExp(searchInput, 'gi');
                const cityName = matched.length > 0 ? matched[0].replace(regex, `<span class="autoComplete">${searchInputCapital}</span>`) : "";
                let firstMatched = matched.length > 0 ? matched[0] : "";
                if (searchInput !== '') {
                    searchInputElement.innerHTML = cityName;
                    this.setState({
                        city: firstMatched
                    });
                } else {
                    searchInputElement.innerHTML = '';
                    this.setState({
                        city: ""
                    });
                }
                if (e.which === 13) {
                    let bolded = `<span class="autoComplete">${firstMatched}</span>`;
                    searchInputElement.innerHTML = bolded;
                    this.setState({
                        city: firstMatched
                    })
                }
            }
        }
    };


    submitInfo = () => {
        let input1City = this.state.city;
        let input2Age = document.querySelector(".candidateInfoAge").value;
        let input3Education = document.querySelector(".candidateInfoEdu").value;
        let input4Job = document.querySelector(".candidateInfoJob").value;
        let sellectInterest = document.querySelector(".candidateInfoDirection").value;
        let checkNumber = 0;
        let check = () => {
            // eslint-disable-next-line
            input1City === "" ? checkNumber : checkNumber++;
            // eslint-disable-next-line
            input2Age === "" ? checkNumber : checkNumber++;
            // eslint-disable-next-line
            input3Education === "" ? checkNumber : checkNumber++;
            // eslint-disable-next-line
            input4Job === "" ? checkNumber : checkNumber++;
        };
        check();
        if (checkNumber === 4) {
            let curp = document.querySelector(".curpNumber").value;
            let curpUpper = curp.toUpperCase();
            let re = /^[a-zA-Z0-9]{18}$/;
            let OK = curpUpper.match(re);
            if(OK) {
                let candidateInfo = {
                    input1City,
                    input2Age,
                    input3Education,
                    input4Job,
                    sellectInterest,
                    curp: curpUpper
                };
                let docRef = db.doc(`users/${curpUpper}`);
                docRef.get()
                    .then(doc => {
                        if (doc.exists) {
                            this.props.addCurp(curpUpper);
                            localStorage.setItem("curp", curpUpper);
                            this.props.progressPageAction();
                        } else {
                            let docRef = db.doc(`users/${curpUpper}`);
                            let docRefClone = dbClone.doc(`users/${curpUpper}`);
                            docRef.set({candidateInfo: candidateInfo});
                            docRefClone.set({candidateInfo: candidateInfo});
                            this.props.addCurp(curpUpper);
                                firebase.auth().signInWithRedirect(provider)
                                    .catch(err => console.error(err))
                        }
                    });
            } else {
                modal.alert({
                    message: 'CURP number is invalid',
                    callback: function () {
                        console.log('curp number is invalid')
                    },
                    okTitle: 'OK'
                })
            }
        } else {
            modal.alert({
                message: "Please fill all the fields",
                callback: () => {
                    console.log('please fill all the fields')
                },
                okTitle: 'OK'
            });
        }
    };



    confirmCurp = () => {
        let curp = document.querySelector(".doneCurp").value;
        let curpUpper = curp.toUpperCase();
        let re = /^[a-zA-Z0-9]{18}$/;
        let OK = curpUpper.match(re);
        if(OK) {
            let docRef = db.doc(`users/${curpUpper}`);
            docRef.get()
                .then(doc => {
                    if (doc.exists) {
                        localStorage.setItem("curp" , curpUpper);
                        this.props.addCurp(curpUpper);
                        localStorage.setItem("curp", curpUpper);
                        this.props.progressPageAction();
                        this.setState({
                            doneAlready: false,
                        });
                    } else {
                        modal.alert({
                            message: "Invalid CURP number",
                            callback: () => {
                                console.log('Invalid CURP number')
                            },
                            okTitle: 'OK'
                        });
                    }
                });
        }  else  {
            modal.alert({
                message: "Invalid CURP number",
                callback: () => {
                    console.log('Invalid CURP number')
                },
                okTitle: 'OK'
            });
        }
    };


    selectAge = () => {
        document.querySelector(".candidateInfoAge").classList.remove("selectColor");
        document.querySelector(".candidateInfoAge").classList.add("selectedValue");
    };
    selectDirection = () => {
        document.querySelector(".candidateInfoDirection").classList.remove("selectColor");
        document.querySelector(".candidateInfoDirection").classList.add("selectedValue");
    };
    educationBold = () => {
        let eduText = document.querySelector(".candidateInfoEdu").value;
        if (eduText === "") {
            document.querySelector(".candidateInfoEdu").classList.remove("autoComplete");
        } else {
            document.querySelector(".candidateInfoEdu").classList.add("autoComplete");
        };
    };
    jobBold = () => {
        let jobText = document.querySelector(".candidateInfoJob").value;
        if (jobText === "") {
            document.querySelector(".candidateInfoJob").classList.remove("autoComplete");
        } else {
            document.querySelector(".candidateInfoJob").classList.add("autoComplete");
        }
    }

    curpBold = () => {
        let curpText = document.querySelector(".curpNumber").value;
        if (curpText === "") {
            document.querySelector(".curpNumber").classList.remove("autoComplete");
        }
        if (JSON.stringify(curpText).length - 2 === 18) {
            document.querySelector(".curpNumber").classList.add("autoComplete");
        } else {
            document.querySelector(".curpNumber").classList.remove("autoComplete");
        }
    };

    openDoneDiv = () => {
        this.setState({
            doneAlready: !this.state.doneAlready,
        })
    };


    render() {
        let opacity={
            "opacity" : `${this.state.opacity}`
        };
        let image = require("./../../content/DF-logo.png");
        let completedText = this.state.doneAlready ? "I haven't completed the quiz" : "I have completed the quiz already";
        return (
            <div className="loginPage">
                <img className="logoLogIn" src={image} alt=""></img>
                <div className="loginDivFlex">
                    <div>
                        <label htmlFor="direction" className="directionLabel">Career Preference</label>
                        <select id="direction" name="direction" className="candidateInfoDirection" onChange={this.selectDirection}>
                            <option value="Dont-know">Don't know</option>
                            <option value="Front-end">Front end</option>
                            <option value="Back-end">Back end</option>
                        </select>
                    </div>
                    <div>
                        <label htmlFor="ageSelect" className="ageLabel">Age</label>
                        <select type="text" id="ageSelect" className="candidateInfoAge" onChange={this.selectAge}></select>
                    </div>
                    <div>
                        <label htmlFor="curpNumber" className="curpLabel">Curp</label>
                        <input type="text"  id="curpNumber" placeholder="Valid curp number" className="curpNumber"
                               onKeyUp={this.curpBold}
                        />
                    </div>
                    <label htmlFor="cancity" className="cityLabel">City</label>
                    <span className="candidateInfoCitySpan"></span>
                    <input type="text" id="cancity" name="cancity" placeholder="Where do you live now ?" className="candidateInfoCity"
                           style={opacity}
                           onKeyUp={this.displayMatches}
                    />
                    <div>
                        <label htmlFor="caneducation" className="eduLabel">Education</label>
                        <input type="text" id="caneducation" name="caneducation" placeholder="Highest level completed" className="candidateInfoEdu"
                               onKeyUp={this.educationBold}
                        />
                    </div>
                    <div className="lastChildDiv">
                        <label htmlFor="lastjob" className="jobLabel">Job</label>
                        <input type="text" id="lastjob" name="lastjob" placeholder="Your last job (if any)" className="candidateInfoJob"
                               onKeyUp={this.jobBold}
                        />
                    </div>
                    <button type="button" className="submitCandidateInfo" onClick={this.submitInfo}>Submit & Start</button>
                    <div className="haveDoneAlready-link-small" onClick={this.openDoneDiv}>{completedText}</div>
                </div>
                <div className="haveDoneAlready-link-big" onClick={this.openDoneDiv}>{completedText}</div>

                {this.state.doneAlready &&
                <div className="haveDoneAlready-div">
                    <input placeholder="CURP number" className="doneCurp"></input>
                    <button className="haveDoneAlready-button" onClick={this.confirmCurp}>Confirm</button>
                </div>
                }
            </div>
        )
    }
}

const mapStatetoProps = (state) => ({
    state: state.questions,
    number: state.order,
    storedAnswers: state.answers,
    email: state.order.userInfo,
    started: state.order.started,
    completed: state.order.completed,
    userInfo: state.order.userInfo,
    answers: state.answers,
    total: state.total.totalPoints,
    curp: state.order.curp,
    infoText: state.infoText,
    questionsTotal: state.order.totalQuestions,
    secondStep : state.secondStage.secondStep,
    interval: state.order,
    loadingCube: state.pages.loadingCube,
    logInPage: state.pages.logInPage,
    progressPage: state.pages.progressPage,
    questions: state.pages.questions,
    secondStepPage: state.pages.secondStep,
    resultsPage: state.pages.resultsPage,
});

const mapDispatchToProps = (dispatch) => ({
    nextQuestion: () => dispatch(nextQuestion()),
    countDownSec: () => dispatch(countDownSec()),
    toogleInterval: (enabled) => dispatch(toogleInterval(enabled)),
    selectedAnswerText: (answer) => dispatch(selectedAnswerText(answer)),
    allAnswers: (answer) => dispatch(allAnswers(answer)),
    clearLocalStorage: () => dispatch(clearLocalStorage()),
    startLogin: () => dispatch(startLogin()),
    startLoginRedirect: () => dispatch(startLoginRedirect()),
    addUserInfo: (data) => dispatch(addUserInfo(data)),
    loadAnswers: (answers) => dispatch(loadAnswers(answers)),
    startAllAnswers: (allAnswersInfo) => dispatch(startAllAnswers(allAnswersInfo)),
    addCurp: (curp) => dispatch(addCurp(curp)),
    addCompleted: () => dispatch(addCompleted()),
    clearOnStart: ({min,total}) => dispatch(clearOnStart({min,total})),
    loadQuestions: (allQustions) => dispatch(loadQuestions(allQustions)),
    totalQuestions:(totalQuestions) => dispatch(totalQuestions(totalQuestions)),
    questionsPage: () => dispatch(questionsPage()),
    loginPage: () => dispatch(loginPage()),
    progressPageAction: () => dispatch(progressPageAction()),
    secondStagePage: () => dispatch(secondStagePage()),
    resultsPageAction: () => dispatch(resultsPageAction())
});


export default connect(mapStatetoProps, mapDispatchToProps)(LoginInfo);


