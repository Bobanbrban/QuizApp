import React from 'react';
import { connect } from 'react-redux';
import ReactInterval from "react-interval";
import CircularProgressbarWithGradient from "./progerssbar";


class TimeOrderNext extends React.Component {
    render() {
        const {questionNumber,min,sec,timeout,enabled,percentage} = this.props.info;
        return (
                <div className="timeOrderNext">
                    <div className="remainingTime">
                        <ReactInterval {...{timeout, enabled}}
                                       callback={() => this.props.countdownInterval()} />{min}:{sec}
                    </div>
                    <div className="questionNumber">
                        <CircularProgressbarWithGradient percentage={percentage} qustionNumber={questionNumber} questionsTotal={this.props.questionsTotal}/>
                    </div>
                    <button className="next_question_button" onClick={this.props.nextQuestion}>{this.props.nextButton()}</button>
                </div>
        )
    }
};

const mapStatetoProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});


export default connect( mapStatetoProps, mapDispatchToProps )(TimeOrderNext);
