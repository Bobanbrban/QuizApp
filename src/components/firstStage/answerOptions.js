import React from 'react';


class AnswerOptions extends React.Component {

    toggle = (id) => {
        let checkedBox = document.querySelectorAll("#checkBox");
        let options = document.querySelectorAll("#option");
        checkedBox.forEach(box => parseInt(box.dataset.index, 10) === parseInt(id, 10) ? box.checked = true : box.checked = false);
        options.forEach((option, i) => parseInt(i, 10) === parseInt(id, 10) ? option.classList.add("checked") : option.classList.remove("checked"));
    };


    render() {
        let background =  this.props.options.background;
        let backgroundExist = typeof background !== 'undefined' && background[1] !== '' ? true : false;
        let image1 =  backgroundExist ? background[1] : '';
        let image2 =  backgroundExist ? background[2] : '';
        let image3 =  backgroundExist ? background[3] : '';
        let image4 =  backgroundExist ? background[4] : '';
        let image5 =  backgroundExist ? background[5] : '';

        let style1 = {backgroundImage: 'url(' + image1 + ')', backgroundSize: 'cover', minHeight : "80px" , minWidth : "80px",
            height: "10vw" , maxHeight: "150px", width: "10vw",maxWidth: "150px",borderRadius: "10px",  marginLeft: "2vw" };
        let style2 = {backgroundImage: 'url(' + image2 + ')', backgroundSize: 'cover', minHeight : "80px" , minWidth : "80px",
            height: "10vw" ,maxHeight: "150px", width: "10vw",maxWidth: "150px",borderRadius: "10px", marginLeft: "2vw"};
        let style3 = {backgroundImage: 'url(' + image3 + ')', backgroundSize: 'cover', minHeight : "80px" , minWidth : "80px" ,
            height: "10vw" ,maxHeight: "150px", width: "10vw",maxWidth: "150px",borderRadius: "10px", marginLeft: "2vw"};
        let style4 = {backgroundImage: 'url(' + image4 + ')', backgroundSize: 'cover', minHeight : "80px" , minWidth : "80px",
            height: "10vw" ,maxHeight: "150px", width: "10vw",maxWidth: "150px",borderRadius: "10px", marginLeft: "2vw"};
        let style5 = {backgroundImage: 'url(' + image5 + ')', backgroundSize: 'cover', minHeight : "80px" , minWidth : "80px",
            height: "10vw" ,maxHeight: "150px", width: "10vw",maxWidth: "150px",borderRadius: "10px", marginLeft: "2vw"};
        return (
            <div className="answerOptions">
                <div className="option1" id="option" onClick={() => this.toggle(0)} style={backgroundExist ? style1 : {}}>
                    <div className="answerText" >
                        {this.props.options[1]}
                    </div>
                    <div className="squaredThree squaredThree1">
                        <input type="checkbox" value="None" id="checkBox" name="check" data-index="0"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
                <div className="option2" id="option" onClick={() => this.toggle(1)} style={backgroundExist ? style2 : {}}>
                    <div className="answerText">
                        {this.props.options[2]}
                    </div>
                    <div className="squaredThree squaredThree2">
                        <input type="checkbox" value="None"  id="checkBox" name="check" data-index="1"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
                <div className="option3" id="option" onClick={() => this.toggle(2)} style={backgroundExist ? style3 : {}}>
                    <div className="answerText">
                        {this.props.options[3]}
                    </div>
                    <div className="squaredThree squaredThree3">
                        <input type="checkbox" value="None"  id="checkBox" name="check" data-index="2"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
                <div className="option4" id="option" onClick={() => this.toggle(3)} style={backgroundExist ? style4 : {}}>
                    <div className="answerText">
                        {this.props.options[4]}
                    </div>
                    <div className="squaredThree squaredThree4">
                        <input type="checkbox" value="None"  id="checkBox" name="check" data-index="3"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
                <div className="option5" id="option" onClick={() => this.toggle(4)} style={backgroundExist ? style5 : {}}>
                    <div className="answerText">
                        {this.props.options[5]}
                    </div>
                    <div className="squaredThree squaredThree5">
                        <input type="checkbox" value="None"  id="checkBox" name="check" data-index="4"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
            </div>
        )
    }
}



export default AnswerOptions;