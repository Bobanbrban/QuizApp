import React from 'react';
import LoadingGif from "./../loadingGif";
import Questions from "./quizQuestions";
import TimeOrderNext from "./timeOrderNext";
import ResultsPage from "./resultsPage";
import SecondStep from "../secondStage/secondStep";
import LoginInfo from "./loginInfo";
import ProgressPage from "../secondStage/progress";
import db from '../../firebase/firebase';
import dbClone from '../../firebase/firebaseClone';
import { firebase } from  "../../firebase/firebase";
import { connect } from 'react-redux';
import {
    nextQuestion,
    countDownSec,
    toogleInterval,
    selectedAnswerText,
    allAnswers,
    clearLocalStorage,
    addUserInfo,
    loadAnswers,
    startAllAnswers,
    addCurp,
    addCompleted,
    clearOnStart,
    loadQuestions,
} from "../../actions/actions"
import { startLogin, startLoginRedirect } from "../../actions/auth";
import { loginPage,progressPageAction,questionsPage,secondStagePage,resultsPageAction } from "../../actions/pagesActions";
import {correctPassword} from "../../actions/adminActions";
import modal from "modals";

class QuestionBodyMAIN extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            selected: 0,
            smallerScreen: false,
            adminTrue: false,
            user: '',
        }
    }
    componentWillMount() {
        let curpUpper = this.props.curp;
        firebase.auth().onAuthStateChanged(user => {
            if (user && curpUpper !== '') {
                let displayName = user.displayName;
                let email = user.email;
                let emailVerified = user.emailVerified;
                let photoURL = user.photoURL;
                let uid = user.uid;
                let phoneNumber = user.phoneNumber;
                let providerData = user.providerData;
                user.getIdToken().then(accessToken => {
                    let user = {
                        displayName: displayName,
                        email: email,
                        emailVerified: emailVerified,
                        phoneNumber: phoneNumber,
                        photoURL: photoURL,
                        uid: uid,
                        accessToken: accessToken,
                        providerData: providerData
                    };
                    this.props.addUserInfo(user);
                    let usersRef = db.doc(`users/${curpUpper}`);
                    usersRef.get()
                        .then(doc => {
                            let userInfo = user;
                            if (doc.exists) {
                                let user = typeof doc.data().valueOf().user !== "undefined" ? doc.data().valueOf().user : '';
                                this.setState({user: user});
                                if (doc.data().valueOf().completed === true) {
                                    this.props.progressPageAction();
                                } else if (typeof doc.data().valueOf().started === "undefined") {
                                    let session = JSON.parse(sessionStorage.getItem('sessionIndex'));
                                    let localSession = JSON.parse(localStorage.getItem('sessionIndex'));
                                    if (localSession === session) {
                                        this.getQuestions(curpUpper, userInfo);
                                    } else {
                                        modal.alert({
                                            message: 'Looks like quiz has been started in other tab or browser ',
                                            callback: function () {
                                                console.log('Looks quiz has been started in other tab or browser ')
                                            },
                                            okTitle: 'OK'
                                        })
                                    }
                                } else if (doc.data().valueOf().started === true && doc.data().valueOf().completed !== true) {
                                    let session =  JSON.parse(sessionStorage.getItem('sessionIndex'));
                                    let localSession = JSON.parse(localStorage.getItem('sessionIndex'));
                                    if(localSession === session) {
                                        let allQuestions = doc.data().valueOf().questions;
                                        this.props.loadQuestions(allQuestions);
                                        let enabled = true;
                                        this.props.toogleInterval(enabled);
                                        this.props.questionsPage();
                                    } else {
                                        modal.alert({
                                        message: 'Looks like quiz has been started in other tab or browser ',
                                            callback: function () {
                                            console.log('Looks quiz has been started in other tab or browser ')
                                        },
                                        okTitle: 'OK'
                                    })
                                    }

                                } else {
                                    let docRef = db.doc('databaseSet/settings');
                                    docRef.get()
                                        .then(async data => {
                                            let min = await data.data().min;
                                            let total = await data.data().total;
                                            this.props.clearOnStart({min, total});
                                            this.props.loginPage();
                                        })
                                        .catch(err => console.error(err));
                                }
                            } else {
                                let docRef = db.doc('databaseSet/settings');
                                docRef.get()
                                    .then(async data => {
                                        let min = await data.data().min;
                                        let total = await data.data().total;
                                        this.props.clearOnStart({min, total});
                                        this.props.loginPage();
                                    })
                                    .catch(err => console.error(err));
                            }
                        });
                });
            } else {
                let docRef = db.doc('databaseSet/settings');
                docRef.get()
                    .then(async data => {
                        let min = await data.data().min;
                        let total = await data.data().total;
                        this.props.clearOnStart({min, total});
                        this.props.loginPage();
                    })
                    .catch(err => console.error(err));
            }
        });
    }

    componentWillUnmount() {
        let enabled = false;
        this.props.toogleInterval(enabled);
        if (this.props.started && !this.props.completed && this.props.number.order < (this.props.questionsTotal - 1) && ((this.props.number.min + this.props.number.sec)!== 0)) {
            this.addAnswer();
            this.props.nextQuestion();
        } else if ( this.props.number.order === (this.props.questionsTotal - 1) || ((this.props.number.min + this.props.number.sec) === 0)) {
            this.addAnswer();
            setTimeout(() => this.props.addCompleted(),0);
            setTimeout(() => this.props.resultsPageAction(),0);
        }
    }


    getQuestions = (curpUpper, user) => {
        fetch('https://us-central1-agavequiz-main.cloudfunctions.net/getRundomQuestions ')
            .then(blob => blob.json())
            .then(async asyncData => {
                let data = await asyncData;
                this.props.clearLocalStorage();
                this.props.loadQuestions(data);
                let enabled = true;
                let docRef = db.doc('databaseSet/settings');
                docRef.get()
                    .then(async data => {
                        let min = await data.data().min;
                        let total = await data.data().total;
                        this.props.clearOnStart({min, total});
                    });
                this.props.toogleInterval(enabled);
                this.props.questionsPage();
                if(user !== undefined) {
                    this.props.addUserInfo(user);
                    let sessionTimestamp = JSON.stringify(new Date().getTime());
                    sessionStorage.setItem('sessionIndex', sessionTimestamp);
                    let checkSession = localStorage.getItem('sessionIndex');
                    if(!checkSession) {
                        localStorage.setItem('sessionIndex', sessionTimestamp);
                    }
                    dbClone.doc(`users/${curpUpper}`)
                        .update({questions: data, started: true, completed: false,progressBarLoaded: false, user: user, sessionTimestamp: sessionTimestamp});
                    db.doc(`users/${curpUpper}`)
                        .update({questions: data, started: true, completed: false,progressBarLoaded: false, user: user, sessionTimestamp: sessionTimestamp})
                        .then(() => console.log("data was set to the database"))
                        .catch(error => console.error("Error adding document: ", error));
                } else  {
                    this.props.addUserInfo(user);
                    db.doc(`users/${curpUpper}`)
                        .update({questions: data, started: true, completed: false,progressBarLoaded: false})
                        .then(() => console.log("data was set to the database"))
                        .catch(error => console.error("Error adding document: ", error));
                }
            });
    };

    nextQuestion = () => {
        if (this.props.started && !this.props.completed && this.props.number.order < (this.props.questionsTotal - 1) && ((this.props.number.min + this.props.number.sec)!== 0)) {
            this.addAnswer();
            this.props.nextQuestion();
        } else if ( this.props.number.order === (this.props.questionsTotal - 1) || ((this.props.number.min + this.props.number.sec) === 0)) {
            this.addAnswer();
            localStorage.setItem("secondStep" , JSON.stringify(true));
            localStorage.setItem("progressBarLoaded" , JSON.stringify(true));
            setTimeout(() => this.props.addCompleted(),0);
            setTimeout(() => this.props.resultsPageAction(),0);
        }
    };


    addAnswer = () => {
        let checkedBox = document.querySelectorAll("#checkBox");
        let qustionNumber = this.props.number.order;
        let ckecked = document.querySelectorAll('#checkBox');
        let checkedArray = [...ckecked];
        let indexes = checkedArray.findIndex(e => e.checked);
        let answerOptions = this.props.state[qustionNumber].answerOptions;
        let backgroundExist = typeof answerOptions.background !== 'undefined' && answerOptions.background[1] !== '' ? true : false;
        let selectedAnswer;
        if(backgroundExist) {
            selectedAnswer = this.props.state[qustionNumber].answerOptions.background[indexes + 1];
        } else {
            selectedAnswer =  this.props.state[qustionNumber].answerOptions[indexes + 1];
        }
        let answer = {
            question: this.props.state[qustionNumber].text,
            correctAnswer: this.props.state[qustionNumber].correctAnswer,
            answer: selectedAnswer,
            questionId: this.props.state[qustionNumber].id,
            points: parseInt(this.props.state[qustionNumber].points, 10),
            qustionOrderNumber: this.props.number.order + 1,
        };
        this.props.selectedAnswerText({
            answer: answer,
        });
        this.props.allAnswers({
            answer: answer,
        });
        checkedBox.forEach(box => box.checked = false);
        let options = document.querySelectorAll("#option");
        options.forEach(option => option.classList.remove("checked"));
    };

    countdownInterval = () => {
        let min = this.props.number.min;
        let sec = this.props.number.sec;
        let parseMin = parseInt(min, 10);
        let parseSec = parseInt(sec, 10);
        if((parseMin + parseSec) > 0) {
            this.props.countDownSec()
        } else {
            let enabled = false;
            this.props.toogleInterval(enabled);
            setTimeout(() => this.nextQuestion(),0);
        }
    };

    next = () => {
        if (!this.props.started && !this.props.completed){
            return  "START"
        } else if (this.props.number.order ===  (this.props.questionsTotal - 1) || ((this.props.number.min + this.props.number.sec) === 0)) {
            return  "SUBMIT"
        } else {
            return "NEXT"
        }
    };

    adminPassword = (e) => {
        e.preventDefault();
        let password = document.querySelector('.adminPasswordInput').value;
        if(password === 'agave123lab' || password === 'slagalica1') {
            this.props.correctPassword('admin');
        }
    };

    render() {
        let min = this.props.number.min;
        let sec = this.props.number.sec < 10 ? "0" + this.props.number.sec : this.props.number.sec;
        let timeout = this.props.interval.timeout;
        let enabled = this.props.interval.enabled;
        let questionNumber = this.props.number.order;
        let questionGraph = typeof this.props.state[questionNumber] === ("undefined") || this.props.state[questionNumber] == null ? false : true;
        let image =  questionGraph ? this.props.state[questionNumber].graphUrl : '';
        let percentage = (((questionNumber + 1)/ this.props.questionsTotal) * 100);
        let imageBackground = require ( `../../content/Preloader_3.gif`);
        let info = {image,questionNumber,questionGraph,imageBackground,min,sec,timeout,enabled,percentage};
        return <div className="body-container">
            { this.props.loadingCube &&
            <LoadingGif image={imageBackground}/>
            }
            { this.props.logInPage &&
            <LoginInfo
                nextQuestion={this.nextQuestion}
                addCompleted={this.props.addCompleted}
                login={this.onSubmit}
                smallerScreen={this.state.smallerScreen}
            />
            }
            { this.props.questions &&
            <Questions
                info={info}
                text={questionGraph ? this.props.state[questionNumber].text : ''}
                options={questionGraph ? this.props.state[questionNumber].answerOptions : []}
                countdownInterval={this.countdownInterval}
                nextQuestion={this.nextQuestion}
                questionsTotal={this.props.questionsTotal}
                nextButton={this.next}
            /> }
            {this.props.questions &&
            <TimeOrderNext
                info={info}
                questionsTotal={this.props.questionsTotal}
                countdownInterval={this.countdownInterval}
                nextQuestion={this.nextQuestion}
                nextButton={this.next}
            /> }
            { this.props.resultsPage && <ResultsPage answers={this.props.answers}/> }
            { this.props.progressPage && <ProgressPage getQuestions={this.getQuestions} /> }
            { this.props.secondStepPage && <SecondStep/> }
        </div>
    }
}

const mapStatetoProps = (state) => ({
    state: state.questions,
    number: state.order,
    storedAnswers: state.answers,
    email: state.order.userInfo,
    started: state.order.started,
    completed: state.order.completed,
    userInfo: state.order.userInfo,
    answers: state.answers,
    total: state.total.totalPoints,
    curp: state.order.curp,
    infoText: state.infoText,
    questionsTotal: state.order.totalQuestions,
    secondStep : state.secondStage.secondStep,
    interval: state.order,
    loadingCube: state.pages.loadingCube,
    logInPage: state.pages.logInPage,
    progressPage: state.pages.progressPage,
    questions: state.pages.questions,
    secondStepPage: state.pages.secondStep,
    resultsPage: state.pages.resultsPage,
    adminTrue: state.questionId.adminPassword,
});

const mapDispatchToProps = (dispatch) => ({
    nextQuestion: () => dispatch(nextQuestion()),
    countDownSec: () => dispatch(countDownSec()),
    toogleInterval: (enabled) => dispatch(toogleInterval(enabled)),
    selectedAnswerText: (answer) => dispatch(selectedAnswerText(answer)),
    allAnswers: (answer) => dispatch(allAnswers(answer)),
    clearLocalStorage: () => dispatch(clearLocalStorage()),
    startLogin: () => dispatch(startLogin()),
    startLoginRedirect: () => dispatch(startLoginRedirect()),
    addUserInfo: (data) => dispatch(addUserInfo(data)),
    loadAnswers: (answers) => dispatch(loadAnswers(answers)),
    startAllAnswers: (allAnswersInfo) => dispatch(startAllAnswers(allAnswersInfo)),
    addCurp: (curp) => dispatch(addCurp(curp)),
    addCompleted: () => dispatch(addCompleted()),
    clearOnStart: ({min,total}) => dispatch(clearOnStart({min,total})),
    loadQuestions: (allQustions) => dispatch(loadQuestions(allQustions)),
    totalQuestions:(totalQuestions) => dispatch(totalQuestions(totalQuestions)),
    questionsPage: () => dispatch(questionsPage()),
    loginPage: () => dispatch(loginPage()),
    progressPageAction: () => dispatch(progressPageAction()),
    secondStagePage: () => dispatch(secondStagePage()),
    resultsPageAction: () => dispatch(resultsPageAction()),
    correctPassword: (text) => dispatch(correctPassword(text)),
});

export default connect(mapStatetoProps, mapDispatchToProps)(QuestionBodyMAIN);
