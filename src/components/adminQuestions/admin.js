import React from 'react';
import { connect } from 'react-redux';
import {
    newQuestion,
    questionId,
    loadAdminQuestions,
    graphUrl,
    loadAllDatabase,
    graphMetadata,
    pathExist,
    correctPassword,
    selectGraphsList,
    graphName
} from '../../actions/adminActions'
import db, { storageRef } from "../../firebase/firebase";
import dbClone, { storageRefClone } from "../../firebase/firebaseClone";
import QuestionsAdminList from './adminQuestions';
import AdminOnly from "./../adminOnly";
import modal from "modals";



class Admin extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
            totalQuestions: 0,
            metadata: {},
            metadataSec: {},
        }
    }

    componentDidMount() {
        let loadSelectImages = () => {
            const userRef = db.doc(`graphCollection/allGraphs`);
            userRef.get().then(data => {
                let graphs = data.data().allMetadata;
                let i;
                for (i = 0; i < graphs.length; i++) {
                    let option = document.createElement("option");
                    option.setAttribute("data-index" ,  `${i}`);
                    option.setAttribute("value" ,  `${i}`);
                    let text = graphs[i].graph;
                    option.innerHTML = `<span>${text}</span>`;
                    document.querySelector('.selectGraph').appendChild(option);
                }
                this.props.selectGraphsList(graphs);
                document.querySelector('.selectGraph').value = 10;
                let metadataIndex = graphs[10];
                let url = metadataIndex.url;
                let graph = metadataIndex.graph;
                let metadataAll = metadataIndex.metadata;
                let metadata = {url,graph,...metadataAll};
                document.querySelector(".graphAdmin").value = graph;
                document.querySelector(".graphUrlAdmin").value = url;
                this.props.graphUrl(url);
                this.props.graphMetadata(metadata);
                this.props.graphName(graph);
            })
        };
        loadSelectImages();


        let questionGroup = document.querySelector(".question-group").value;
        this.props.questionId({questionGroup});


        let loadQuestionsDatabase = () => {
            let questionArray = [];
            let userRef = db.collection(`team`);
            userRef.get()
                .then(async doc => {
                    doc.forEach( doc => {
                        questionArray.push(doc.data().question);
                    });
                    this.props.loadAdminQuestions(await questionArray);
                })
        };
        loadQuestionsDatabase();

        let getAllQuestions = () => {
            if(this.props.adminOrTeam === "admin" || this.props.adminOrTeam === "agaveTeam") {
                fetch('https://us-central1-agavequiz-main.cloudfunctions.net/adminAllQuestions')
                    .then(blob => blob.json())
                    .then(questions => {
                        this.props.loadAllDatabase(questions);
                    })
                    .catch(error => console.error(error));
            }
        };
        getAllQuestions();

        let loadDataBaseSet = () => {
            let docRef = db.doc('databaseSet/settings');
            docRef.get().then(data => {
                document.querySelector(".selectLogical").value = data.data().logical;
                document.querySelector(".selectComplex").value = data.data().complex;
                document.querySelector(".selectNumerical").value = data.data().numerical;
                document.querySelector(".selectSpecial").value = data.data().special;
                document.querySelector(".selectPattern").value = data.data().pattern;
                document.querySelector(".selectMin").value = data.data().min;
                let total = data.data().total;
                this.setState({
                    totalQuestions: total
                })
            })
        };
        loadDataBaseSet();
    }


    //componentDidMount finish here -----------------------------------------------------------------

    selectGraphDatabase = (e) => {
        let selectedIndex = e.target.selectedOptions[0].value;
        if(selectedIndex === 'none') {
            let metadata = {};
            document.querySelector(".graphAdmin").value = '';
            document.querySelector(".graphUrlAdmin").value = '';
            this.props.graphMetadata(metadata);
            let url = '';
            this.props.graphUrl(url);
            let graph = '';
            this.props.graphName(graph);
        } else {
            let metadataIndex = this.props.allMetadata[selectedIndex];
            let url = metadataIndex.url;
            let graph = metadataIndex.graph;
            let metadataAll = metadataIndex.metadata;
            let metadata = {url,graph,...metadataAll};
            document.querySelector(".graphAdmin").value = graph;
            document.querySelector(".graphUrlAdmin").value = url;
            this.props.graphUrl(url);
            this.props.graphMetadata(metadata);
            this.props.graphName(graph);
        }
    };


    adminPassword = (e) => {
        e.preventDefault();
        let password = document.querySelector('.adminPasswordInput').value;
        if(password === 'agave123lab') {
            this.props.correctPassword('admin');
            fetch('https://us-central1-agavequiz.cloudfunctions.net/adminAllQuestions')
                .then(blob => blob.json())
                .then(questions => {
                    this.props.loadAllDatabase(questions);
                })
                .catch(error => console.error(error));
        } else if(password === 'slagalica1') {
            this.props.correctPassword('admin');
            fetch('https://us-central1-agavequiz.cloudfunctions.net/adminAllQuestions')
                .then(blob => blob.json())
                .then(questions => {
                    this.props.loadAllDatabase(questions);
                })
                .catch(error => console.error(error));
        } else {
            modal.alert({
                message: `Wrong password`,
                callback: function () {
                    console.log('Wrong password');
                },
                okTitle: 'OK'
            });
        }
    };

    questionIdChange = () => {
        let questionGroup = document.querySelector(".question-group").value;
        this.props.questionId({questionGroup});
    };

    pushNewQuestion = () => {
        if(this.props.adminOrTeam === "admin") {
            let questionGroup = document.querySelector(".question-group").value || '';
            let questionText = document.querySelector(".question-textAdmin").value || '';
            let answerOption1 = document.querySelector(".answer-option-1").value || '';
            let answerOption2 = document.querySelector(".answer-option-2").value || '';
            let answerOption3 = document.querySelector(".answer-option-3").value || '';
            let answerOption4 = document.querySelector(".answer-option-4").value || '';
            let answerOption5 = document.querySelector(".answer-option-5").value || '';

            let background1 = document.querySelector(".background-option-1").value || '';
            let background2 = document.querySelector(".background-option-2").value || '';
            let background3 = document.querySelector(".background-option-3").value || '';
            let background4 = document.querySelector(".background-option-4").value || '';
            let background5 = document.querySelector(".background-option-5").value || '';


            let backgroundOptions = {1: background1, 2: background2, 3: background3, 4: background4, 5: background5};
            let option = {
                1: answerOption1,
                2: answerOption2,
                3: answerOption3,
                4: answerOption4,
                5: answerOption5,
                background: backgroundOptions
            };

            let correctAnswer = document.querySelector(".correctAnswerAdmin").value || '';
            let pointsValue = document.querySelector(".points-value").value || '';
            let questionId = this.props.id;
            let graph = document.querySelector(".graphAdmin").value || '';
            let graphUrl = document.querySelector(".graphUrlAdmin").value || '';
            let graphMetadata = {
                graphName: this.props.metadata.fullPath || '',
                fullPath: this.props.metadata.fullPath || '',
                graphShortName: this.props.metadata.graphShortName || '',
                timestamp: this.props.metadata.timestamp || '',
                contentType: this.props.metadata.contentType || '',
                created: this.props.metadata.created || '',
                graphFolder: 'agaveTeam',
            };
            let questionObject = {
                text: questionText,
                answerOptions: option,
                correctAnswer,
                graph,
                questionId,
                points: parseInt(pointsValue, 10),
                selectedAnswer: '',
                questionGroup,
                graphUrl,
                graphMetadata,
                path: this.props.path
            } || {};
            setTimeout(() => this.props.newQuestion(questionObject), 0);
            console.log(questionObject);
        } else {
            modal.alert({
                message: `Ask admin for permition`,
                callback: function () {
                    console.log('Wrong password');
                },
                okTitle: 'OK'
            });
        }
    };

    loadGraph = () => {
        let url = document.querySelector('.graphUrlAdmin').value;
        this.props.graphUrl(url);
    };

    clearAll = () => {
        this.props.pathExist('');
        document.querySelector('.questionDivAdmin').reset();
        let url = '';
        this.props.graphUrl(url);
        let metadata = {};
        this.props.graphMetadata(metadata);
        let graph = '';
        this.props.graphName(graph);
        let docRef = db.doc('databaseSet/settings');
        docRef.get().then(data => {
            document.querySelector(".selectLogical").value = data.data().logical;
            document.querySelector(".selectComplex").value = data.data().complex;
            document.querySelector(".selectNumerical").value = data.data().numerical;
            document.querySelector(".selectSpecial").value = data.data().special;
            document.querySelector(".selectPattern").value = data.data().pattern;
            document.querySelector(".selectMin").value = data.data().min;
        })
    };


    uploadFile = (e) => {
        let file = e.target.files[0];
        console.log(file);
        let uploader = document.querySelector("#uploader");
        let timestamp = new Date().getTime();
        let storageFile = storageRef.ref(`agaveTeam/${timestamp}${file.name}`);
        let storageFileClone = storageRefClone.ref(`agaveTeam/${timestamp}${file.name}`);
        let task = storageFile.put(file);
        storageFileClone.put(file);
        task.on("state_changed",
            function progress(snapshot) {
                document.querySelector(".graphAdmin").value = `${file.name}`;
                uploader.value = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            function error (err) {
                modal.alert({
                    message: `${err}`,
                    callback: function () {
                        console.error('err');
                    },
                    okTitle: 'OK'
                });
                console.log("error", err);
            },
            (() => {
                task.snapshot.ref.getMetadata().then(data => {
                    let metadata = {
                        fullPath: data.fullPath,
                        graphName: data.name,
                        contentType: data.contentType,
                        created: data.timeCreated,
                        timestamp: data.generation,
                        graphShortName: file.name
                    };
                    this.props.graphMetadata(metadata);
                    this.setState(metadata);
                    modal.alert({
                        message: `Graph has been uploaded`,
                        callback: function () {
                            console.log('Graph has been uploaded');
                        },
                        okTitle: 'OK'
                    });
                });
                task.snapshot.ref.getDownloadURL()
                    .then(downloadURL => {
                        document.querySelector(".graphUrlAdmin").value = downloadURL;
                        let addUrlMetadata = () => {
                            let graphMetadata = {
                                graphName: this.props.metadata.fullPath,
                                fullPath: this.props.metadata.fullPath,
                                graphShortName: this.props.metadata.graphShortName,
                                timestamp: this.props.metadata.timestamp,
                                contentType: this.props.metadata.contentType,
                                created: this.props.metadata.created,
                                graphFolder: 'agaveTeam',
                            };
                            const userRef = db.doc(`graphCollection/allGraphs`);
                            const userRefClone = dbClone.doc(`graphCollection/allGraphs`);
                            userRef.get().then(data => {
                                let graphs = data.data().allMetadata;
                                let metadata = {
                                    graph: this.props.metadata.graphShortName,
                                    url: downloadURL,
                                    metadata: graphMetadata
                                };
                                let newArray = {allMetadata: [...graphs, metadata]};
                                userRef.set(newArray);
                                userRefClone.set(newArray);
                            });
                            this.props.graphUrl(downloadURL);
                        };
                        setTimeout(addUrlMetadata, 0);
                    });
            }));

    };

    uploadBatch = (e) => {
        let files = e.target.files;
        let timestamp = new Date().getTime();
        if(files.length === 5) {
            let uploaderBackground = document.querySelector("#uploaderBackground");
            let file1 = files[0];
            console.log(file1.lastModified);
            let storageFile1 = storageRef.ref(`agaveTeam/${timestamp}${file1.name}`);
            let task1 = storageFile1.put(file1);

            let file2 = files[1];
            let storageFile2 = storageRef.ref(`agaveTeam/${timestamp}${file2.name}`);
            let task2 = storageFile2.put(file2);

            let file3 = files[2];
            let storageFile3 = storageRef.ref(`agaveTeam/${timestamp}${file3.name}`);
            let task3 = storageFile3.put(file3);

            let file4 = files[3];
            let storageFile4 = storageRef.ref(`agaveTeam/${timestamp}${file4.name}`);
            let task4 = storageFile4.put(file4);

            let file5 = files[4];
            let storageFile5 = storageRef.ref(`agaveTeam/${timestamp}${file5.name}`);
            let task5 = storageFile5.put(file5);
            task1.on("state_changed",
                function progress(snapshot) {
                    console.log("task1", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task1.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-1").value = downloadURL;
                            this.setState({
                                backgroundUrl1: downloadURL
                            });
                        });
                }));
            task2.on("state_changed",
                function progress(snapshot) {
                    console.log("task2", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task2.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-2").value = downloadURL;
                            this.setState({
                                backgroundUrl2: downloadURL
                            });
                        });
                }));
            task3.on("state_changed",
                function progress(snapshot) {
                    console.log("task3", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task3.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-3").value = downloadURL;
                            this.setState({
                                backgroundUrl3: downloadURL
                            });
                        });
                }));
            task4.on("state_changed",
                function progress(snapshot) {
                    console.log("task4", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task4.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-4").value = downloadURL;
                            this.setState({
                                backgroundUrl4: downloadURL
                            });
                        });
                }));
            task5.on("state_changed",
                function progress(snapshot) {
                    uploaderBackground.value = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task5.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-5").value = downloadURL;
                            this.setState({
                                backgroundUrl5: downloadURL
                            });
                        });
                    modal.alert({
                        message: `All files has been uploaded`,
                        callback: function () {
                            console.log('does not have 5 files');
                        },
                        okTitle: 'OK'
                    });
                }));
        } else {
            modal.alert({
                message: `Please select 5 files at ones`,
                callback: function () {
                    console.log('does not have 5 files');
                },
                okTitle: 'OK'
            })
        }
    };

    sumTotalQuestions = () => {
        let logical = document.querySelector(".selectLogical").value;
        let complex = document.querySelector(".selectComplex").value;
        let numerical = document.querySelector(".selectNumerical").value;
        let special = document.querySelector(".selectSpecial").value;
        let pattern = document.querySelector(".selectPattern").value;
        let total = (parseInt(logical, 10) + parseInt(complex, 10) + parseInt(numerical, 10) + parseInt(special, 10) + parseInt(pattern, 10));
        this.setState({
            totalQuestions: total
        })
    };

    correctAnswer = (e) => {
        let index = e.target.dataset.index;
        let value = document.querySelector(`.answer-option-${index}`).value || '';
        document.querySelector('.correctAnswerAdmin').value = value;
    };

    correctBackground = (e) => {
        let index = e.target.dataset.index;
        let value = document.querySelector(`.background-option-${index}`).value || '';
        document.querySelector('.correctAnswerAdmin').value = value;
    };

    saveSettings = () => {
        if(this.props.adminOrTeam === "admin") {
            let logical = document.querySelector(".selectLogical").value;
            let complex = document.querySelector(".selectComplex").value;
            let numerical = document.querySelector(".selectNumerical").value;
            let special = document.querySelector(".selectSpecial").value;
            let pattern = document.querySelector(".selectPattern").value;
            let min = document.querySelector(".selectMin").value;
            let total = (parseInt(logical, 10) + parseInt(complex, 10) + parseInt(numerical, 10) + parseInt(special, 10) + parseInt(pattern, 10));
            let docRef = db.doc('databaseSet/settings');
            let docRefClone = dbClone.doc('databaseSet/settings');
            docRef.update({logical, complex, numerical, special, pattern, total, min})
                .then(() => {
                    modal.alert({
                        message: "Settings has been updated",
                        callback: function () {
                            console.log("Question groups settings has been updated")
                        },
                        okTitle: 'OK'
                    })
                })
                .catch(err => {
                    console.error(err)
                });
            docRefClone.update({logical, complex, numerical, special, pattern, total, min})
                .then(() => {
                    console.log("Question groups settings in clone database has been updated")
                })
                .catch(err => {
                    console.error(err)
                })
        } else {
            modal.alert({
                message: `Ask admin for permission`,
                callback: function () {
                    console.log('Wrong password');
                },
                okTitle: 'OK'
            });
        }
    };


    render() {
        return (
            <div className="adminPage">
                {!this.props.adminTrue && <AdminOnly adminPassword={this.adminPassword} />}
                <div className="questionsInputsAdmin">
                    <form className="questionDivAdmin">
                        <div className="topBlock">
                            <div className="questionTextInfo">
                                <span className="questionIdAdmin">Id {this.props.id} </span>
                                <input type="text" className="question-textAdmin" placeholder="question text" required/>
                                <select type="text" className="question-group" placeholder="question-group" onChange={this.questionIdChange}>
                                    <option className="selectedGroup" value="logical">logical</option>
                                    <option className="selectedGroup" value="numerical">numerical</option>
                                    <option className="selectedGroup" value="complex">complex</option>
                                    <option className="selectedGroup" value="pattern">pattern</option>
                                    <option className="selectedGroup" value="special">special</option>
                                </select>
                                <input type="text" className="correctAnswerAdmin" placeholder="correct answer text" required/>
                                <select type="text" className="points-value" placeholder="points-value">
                                    <option value="4">4 points</option>
                                    <option value="8">8 points</option>
                                </select>
                                <input type="text" className="graphAdmin" placeholder="graph-name"/>
                                <button type="button" className="loadGraph" onClick={this.loadGraph}>Load graph</button>
                                <input type="text" className="graphUrlAdmin" placeholder="graphUrl"/>

                                <div className="fifthLine">
                                    <input type="file" id="fileButton" className="uploadFileInput" onChange={this.uploadFile}/>
                                    <button type="button" className="uploadGraph">Upload graph</button>
                                    <progress value="0" max="100" id="uploader">0%</progress>
                                    <input type="file" id="fileButton"  className="backgroungInput" multiple onChange={this.uploadBatch}/>
                                    <button type="button" className="backgroungGraphs">Background</button>
                                    <progress value="0" max="100" id="uploaderBackground">0%</progress>
                                    <select type="text" className="selectGraph" onChange={this.selectGraphDatabase}>
                                        <option value="none">Choose graph</option>
                                    </select>
                                    <a className="uploadLink" href="https://drive.google.com/drive/u/2/folders/17Zk8ToTHEhl_hirprcNxjRIPsWsvqNe6" target="_blank" rel="noopener noreferrer">Drive</a>
                                    <a className="uploadLink" href="https://www.canva.com/design/DAC7TGDsT1Q/share?role=EDITOR&token=DP3uV6IKzZaQH-iWyffHlQ&utm_content=DAC7TGDsT1Q&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton" target="_blank" rel="noopener noreferrer">Template</a><br/>
                                </div>
                            </div>
                            <img className="graphImageAdmin" src={this.props.url} alt=""/>
                        </div>



                        <div className="optionsSettings">

                            <div className="onlyOptions">
                                <span className="span-option-1" data-index="1" onClick={this.correctAnswer}>CA1</span>
                                <input type="text" className="answer-option-1" name="options" placeholder="answer-option-1" />
                                <span className="span-option-2" data-index="2" onClick={this.correctAnswer}>CA2</span>
                                <input type="text" className="answer-option-2" name="options" placeholder="answer-option-2" />
                                <span className="span-option-3" data-index="3" onClick={this.correctAnswer}>CA3</span>
                                <input type="text" className="answer-option-3" name="options" placeholder="answer-option-3" />
                                <span className="span-option-4" data-index="4" onClick={this.correctAnswer}>CA4</span>
                                <input type="text" className="answer-option-4" name="options" placeholder="answer-option-4" />
                                <span className="span-option-5" data-index="5" onClick={this.correctAnswer}>CA5</span>
                                <input type="text" className="answer-option-5" name="options" placeholder="answer-option-5" /> <br/>

                                <span className="span-background-1" data-index="1" onClick={this.correctBackground}>CB1</span>
                                <input type="text" className="background-option-1" name="background" placeholder="img-option-1" data-index="1"/>
                                <span className="span-background-2" data-index="2" onClick={this.correctBackground}>CB2</span>
                                <input type="text" className="background-option-2" name="background" placeholder="img-option-2" data-index="2"/>
                                <span className="span-background-3" data-index="3" onClick={this.correctBackground}>CB3</span>
                                <input type="text" className="background-option-3" name="background" placeholder="img-option-3" data-index="3"/>
                                <span className="span-background-4" data-index="4" onClick={this.correctBackground}>CB4</span>
                                <input type="text" className="background-option-4" name="background" placeholder="img-option-4" data-index="4"/>
                                <span className="span-background-5" data-index="5" onClick={this.correctBackground}>CB5</span>
                                <input type="text" className="background-option-5" name="background" placeholder="img-option-5" data-index="5"/> <br/>

                                <div className="onlySelectButtons">
                                    <select type="text" className="selectLogical" onChange={this.sumTotalQuestions}>
                                        <option value="0">0 logical</option>
                                        <option value="1">1 logical</option>
                                        <option value="2">2 logical</option>
                                        <option value="3">3 logical</option>
                                        <option value="4">4 logical</option>
                                        <option value="5">5 logical</option>
                                        <option value="6">6 logical</option>
                                        <option value="7">7 logical</option>
                                        <option value="8">8 logical</option>
                                        <option value="9">9 logical</option>
                                        <option value="10">10 logical</option>
                                        <option value="11">11 logical</option>
                                        <option value="12">12 logical</option>
                                        <option value="13">13 logical</option>
                                        <option value="14">14 logical</option>
                                        <option value="15">15 logical</option>
                                    </select>
                                    <select type="text" className="selectNumerical" onChange={this.sumTotalQuestions}>
                                        <option value="0">0 numerical</option>
                                        <option value="1">1 numerical</option>
                                        <option value="2">2 numerical</option>
                                        <option value="3">3 numerical</option>
                                        <option value="4">4 numerical</option>
                                        <option value="5">5 numerical</option>
                                        <option value="6">6 numerical</option>
                                        <option value="7">7 numerical</option>
                                        <option value="8">8 numerical</option>
                                        <option value="9">9 numerical</option>
                                        <option value="10">10 numerical</option>
                                        <option value="11">11 numerical</option>
                                        <option value="12">12 numerical</option>
                                        <option value="13">13 numerical</option>
                                        <option value="14">14 numerical</option>
                                        <option value="15">15 numerical</option>
                                    </select>
                                    <select type="text" className="selectSpecial" onChange={this.sumTotalQuestions}>
                                        <option value="0">0 special</option>
                                        <option value="1">1 special</option>
                                        <option value="2">2 special</option>
                                        <option value="3">3 special</option>
                                        <option value="4">4 special</option>
                                        <option value="5">5 special</option>
                                        <option value="6">6 special</option>
                                        <option value="7">7 special</option>
                                        <option value="8">8 special</option>
                                        <option value="9">9 special</option>
                                        <option value="10">10 special</option>
                                        <option value="11">11 special</option>
                                        <option value="12">12 special</option>
                                        <option value="13">13 special</option>
                                        <option value="14">14 special</option>
                                        <option value="15">15 special</option>
                                    </select>
                                    <select type="text" className="selectComplex" onChange={this.sumTotalQuestions}>
                                        <option value="0">0 complex</option>
                                        <option value="1">1 complex</option>
                                        <option value="2">2 complex</option>
                                        <option value="3">3 complex</option>
                                        <option value="4">4 complex</option>
                                        <option value="5">5 complex</option>
                                        <option value="6">6 complex</option>
                                        <option value="7">7 complex</option>
                                        <option value="8">8 complex</option>
                                        <option value="9">9 complex</option>
                                        <option value="10">10 complex</option>
                                        <option value="11">11 complex</option>
                                        <option value="12">12 complex</option>
                                        <option value="13">13 complex</option>
                                        <option value="14">14 complex</option>
                                        <option value="15">15 complex</option>
                                    </select>
                                    <select type="text" className="selectPattern" onChange={this.sumTotalQuestions}>
                                        <option value="0">0 pattern</option>
                                        <option value="1">1 pattern</option>
                                        <option value="2">2 pattern</option>
                                        <option value="3">3 pattern</option>
                                        <option value="4">4 pattern</option>
                                        <option value="5">5 pattern</option>
                                        <option value="6">6 pattern</option>
                                        <option value="7">7 pattern</option>
                                        <option value="8">8 pattern</option>
                                        <option value="9">9 pattern</option>
                                        <option value="10">10 pattern</option>
                                        <option value="11">11 pattern</option>
                                        <option value="12">12 pattern</option>
                                        <option value="13">13 pattern</option>
                                        <option value="14">14 pattern</option>
                                        <option value="15">15 pattern</option>
                                    </select>
                                    <span className="questionsTotal">{this.state.totalQuestions} total</span>
                                    <select type="text" className="selectMin">
                                        <option value="20">20 minutes</option>
                                        <option value="25">25 minutes</option>
                                        <option value="30">30 minutes</option>
                                        <option value="35">35 minutes</option>
                                        <option value="40">40 minutes</option>
                                        <option value="45">45 minutes</option>
                                        <option value="50">50 minutes</option>
                                        <option value="55">55 minutes</option>
                                        <option value="60">60 minutes</option>
                                    </select>
                                    <button type="button" className="savaDatabase" onClick={this.saveSettings}>Save</button>
                                    <button type="button" className="submitAdmin" onClick={this.pushNewQuestion}>Add Question</button>
                                    <button type="button" className="clearAll" onClick={this.clearAll}>Clear All</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <QuestionsAdminList/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    id: state.questionId.adminId,
    questions: state.questionId.questions,
    url: state.questionId.graphUrl,
    mainBoolean: state.questionId.mainDatabase,
    graph: state.questionId.graph,
    metadata: state.questionId.graphMetadata,
    path: state.questionId.path,
    adminTrue: state.questionId.adminPassword,
    allMetadata: state.questionId.allMetadata,
    adminOrTeam: state.questionId.adminOrTeam,
});

const mapDispatchToProps = (dispatch) => ({
    newQuestion: (questionObject) => dispatch(newQuestion(questionObject)),
    questionId: (group) => dispatch(questionId(group)),
    loadAdminQuestions: (allQuestions) => dispatch(loadAdminQuestions(allQuestions)),
    graphUrl: (url) => dispatch(graphUrl(url)),
    loadAllDatabase: (questions) => dispatch(loadAllDatabase(questions)),
    graphMetadata: (metadata) => dispatch(graphMetadata(metadata)),
    pathExist: (path) => dispatch(pathExist(path)),
    correctPassword: (text) => dispatch(correctPassword(text)),
    selectGraphsList: (graphs) => dispatch(selectGraphsList(graphs)),
    graphName: (graph) => dispatch(graphName(graph)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Admin);