import React from 'react';
import { connect } from 'react-redux';
import QuestionElement from "./questionElement";
import { mainDatabaseBoolean } from '../../actions/adminActions'



class QuestionsAdminList extends React.Component {
               constructor(props) {
                   super(props);
                   this.state = {
                       dataBase: 'questionsTeam',
                   }
               }

               componentDidMount() {
                  let setDatabase = () => {
                       let data = document.querySelector('.selectDatabase').value;
                       this.setState({
                           dataBase: data
                       })
                   };
                   setDatabase();
               }

               onChangeSelection = () => {
                   let data = document.querySelector('.selectDatabase').value;
                   this.props.mainDatabaseBoolean();
                   this.setState({
                       dataBase: data
                   })
               };

               render() {
                   let questions = this.props.questionsTeam;
                   let questionsMain = this.props.mainDatabase;
                   let resultsList;
                   resultsList = questions.map((question, i) => {
                       return (
                           <QuestionElement
                               question={question}
                               i={i}
                               key={i}
                           />
                       )
                   });
                   let mainDatabase;
                   mainDatabase = questionsMain.map((question, i) => {
                       return (
                           <QuestionElement
                               question={question}
                               i={i}
                               key={i}
                           />
                       )
                   });
                   return (
                       <div className="resultsBottom">
                           <select className="selectDatabase" onChange={this.onChangeSelection}>
                               <option value="mainDatabase">Main database</option>
                               <option value="questionsTeam">Questions team</option>
                           </select>
                           <ul className="resultsListAdmin">
                               <li className="listNames">
                                   <span>order</span><span>question text</span><span>correct answer</span><span>graph name</span><span>actions</span>
                               </li>
                               {this.state.dataBase === 'questionsTeam' ? resultsList : mainDatabase}
                           </ul>
                       </div>
                   )
               }
}

const mapStatetoProps = (state) => ({
    questionsTeam: state.questionId.questions,
    mainDatabase: state.questionId.questionsDatabase
});

const mapDispatchToProps = (dispatch) => ({
    mainDatabaseBoolean: () => dispatch(mainDatabaseBoolean()),
});


export default connect( mapStatetoProps, mapDispatchToProps )(QuestionsAdminList);