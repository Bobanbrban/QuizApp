import React from 'react';
import { connect } from 'react-redux';
import { storageRef } from "../../firebase/firebase";
import { newQuestionTeam } from '../../actions/fromTeamAdmin';
import {
    graphMetadata,
    graphName,
    graphUrl,
    selectGraphsList
} from "../../actions/adminActions";
import modal from "modals";
import db from "../../firebase/firebase";



class AgaveTeam extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount () {
        let loadSelectImages = () => {
            const userRef = db.doc(`graphCollection/allGraphs`);
            userRef.get().then(data => {
                let graphs = data.data().allMetadata;
                let i;
                for (i = 0; i < graphs.length; i++) {
                    let option = document.createElement("option");
                    option.setAttribute("data-index" ,  `${i}`);
                    option.setAttribute("value" ,  `${i}`);
                    let text = graphs[i].graph;
                    option.innerHTML = `<span>${text}</span>`;
                    document.querySelector('.selectGraphTeam').appendChild(option);
                }
                this.props.selectGraphsList(graphs);
                document.querySelector('.selectGraphTeam').value = 10;
                let metadataIndex = graphs[10];
                let url = metadataIndex.url;
                let graph = metadataIndex.graph;
                let metadataAll = metadataIndex.metadata;
                let metadata = {url,graph,...metadataAll};
                document.querySelector(".graph").value = graph;
                this.props.graphUrl(url);
                this.props.graphMetadata(metadata);
                this.props.graphName(graph);
            })
        };
        loadSelectImages();
    }


    selectGraphDatabase = (e) => {
        let selectedIndex = e.target.selectedOptions[0].value;
        if(selectedIndex === 'none') {
            let metadata = {};
            document.querySelector(".graph").value = '';
            this.props.graphMetadata(metadata);
            let url = '';
            this.props.graphUrl(url);
            let graph = '';
            this.props.graphName(graph);
        } else {
            let metadataIndex = this.props.allMetadata[selectedIndex];
            let url = metadataIndex.url;
            let graph = metadataIndex.graph;
            let metadataAll = metadataIndex.metadata;
            let metadata = {url,graph,...metadataAll};
            document.querySelector(".graph").value = graph;
            this.props.graphUrl(url);
            this.props.graphMetadata(metadata);
            this.props.graphName(graph);
        }
    };


    pushNewQuestion = () => {
        let questionText = document.querySelector(".question-text").value || '';
        let answerOption1 = document.querySelector(".answer-option-1").value || '';
        let answerOption2 = document.querySelector(".answer-option-2").value || '';
        let answerOption3 = document.querySelector(".answer-option-3").value || '';
        let answerOption4 = document.querySelector(".answer-option-4").value || '';
        let answerOption5 = document.querySelector(".answer-option-5").value || '';

        let background1 = document.querySelector(".background-option-1").value || '';
        let background2 = document.querySelector(".background-option-2").value || '';
        let background3 = document.querySelector(".background-option-3").value || '';
        let background4 = document.querySelector(".background-option-4").value || '';
        let background5 = document.querySelector(".background-option-5").value || '';


        let backgroundOptions = {1: background1 , 2: background2, 3: background3 , 4: background4, 5: background5};
        let option = {1: answerOption1 , 2: answerOption2, 3: answerOption3 , 4: answerOption4, 5: answerOption5, background: backgroundOptions };

        let correctAnswer = document.querySelector(".correctAnswer").value || '';
        let graph = document.querySelector(".graph").value || '';
        let graphMetadata = {
            graphName:this.props.metadata.fullPath,
            fullPath: this.props.metadata.fullPath,
            contentType: this.props.metadata.contentType,
            created: this.props.metadata.created,
            timestamp: this.props.metadata.timestamp,
            graphShortName: this.props.metadata.graphShortName,
            graphFolder: 'agaveTeam',
        };
        let questionObject = {text : questionText, answerOptions : option, correctAnswer, graph, selectedAnswer: '', graphUrl: this.props.url, graphMetadata, path: this.props.path } || {};
        setTimeout(() => this.props.newQuestionTeam(questionObject), 0);
    };

    clearAll = () => {
        let url = '';
        this.props.graphUrl(url);
        let metadata = {};
        this.props.graphMetadata(metadata);
        let graph = '';
        this.props.graphName(graph);
        document.querySelector(".graph").value = '';
        document.querySelector('.questionDiv').reset();
    };

    uploadFile = (e) => {
        let file = e.target.files[0];
        let timestamp = new Date().getTime();
        let uploader = document.querySelector("#uploader");
        let storageFile = storageRef.ref(`agaveTeam/${timestamp}${file.name}`);
        let task = storageFile.put(file);
        task.on("state_changed",
            function progress(snapshot) {
                document.querySelector(".graph").value = `${file.name}`;
                uploader.value = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            function error (err) {
                modal.alert({
                    message: `${err}`,
                    callback: function () {
                        console.error('err');
                    },
                    okTitle: 'OK'
                });
                console.log("error", err);
            },
            (() => {
                task.snapshot.ref.getMetadata().then(data => {
                    let metadata = {
                        fullPath: data.fullPath,
                        graphName: data.name,
                        contentType: data.contentType,
                        created: data.timeCreated,
                        timestamp: data.generation,
                        graphShortName: file.name
                    };
                    this.props.graphMetadata(metadata);
                    modal.alert({
                        message: `Image has been uploaded`,
                        callback: function () {
                            console.log('Download URL')
                        },
                        okTitle: 'OK'
                    })
                });
                task.snapshot.ref.getDownloadURL()
                    .then(downloadURL => {
                        this.props.graphUrl(downloadURL);
                        let addUrlMetadata = () => {
                            let graphMetadata = {
                                graphName: this.props.metadata.fullPath,
                                fullPath: this.props.metadata.fullPath,
                                graphShortName: this.props.metadata.graphShortName,
                                timestamp: this.props.metadata.timestamp,
                                contentType: this.props.metadata.contentType,
                                created: this.props.metadata.created,
                                graphFolder: 'agaveTeam',
                            };
                            const userRef = db.doc(`graphCollection/allGraphs`);
                            userRef.get().then(data => {
                                let graphs = data.data().allMetadata;
                                let metadata = {
                                    graph: this.props.metadata.graphShortName,
                                    url: downloadURL,
                                    metadata: graphMetadata
                                };
                                let newArray = {allMetadata: [...graphs, metadata]};
                                userRef.set(newArray);
                            })
                        };
                        setTimeout(addUrlMetadata, 0);
                    });
            }));
    };

    uploadBatch = (e) => {
        let files = e.target.files;
        let timestamp = new Date().getTime();
        if(files.length === 5) {
            let uploaderBackground = document.querySelector("#uploaderBackground");
            let file1 = files[0];
            console.log(file1.lastModified);
            let storageFile1 = storageRef.ref(`agaveTeam/${timestamp}${file1.name}`);
            let task1 = storageFile1.put(file1);

            let file2 = files[1];
            let storageFile2 = storageRef.ref(`agaveTeam/${timestamp}${file2.name}`);
            let task2 = storageFile2.put(file2);

            let file3 = files[2];
            let storageFile3 = storageRef.ref(`agaveTeam/${timestamp}${file3.name}`);
            let task3 = storageFile3.put(file3);

            let file4 = files[3];
            let storageFile4 = storageRef.ref(`agaveTeam/${timestamp}${file4.name}`);
            let task4 = storageFile4.put(file4);

            let file5 = files[4];
            let storageFile5 = storageRef.ref(`agaveTeam/${timestamp}${file5.name}`);
            let task5 = storageFile5.put(file5);
            task1.on("state_changed",
                function progress(snapshot) {
                    console.log("task1", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task1.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-1").value = downloadURL;
                            this.setState({
                                backgroundUrl1: downloadURL
                            });
                        });
                }));
            task2.on("state_changed",
                function progress(snapshot) {
                    console.log("task2", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task2.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-2").value = downloadURL;
                            this.setState({
                                backgroundUrl2: downloadURL
                            });
                        });
                }));
            task3.on("state_changed",
                function progress(snapshot) {
                    console.log("task3", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task3.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-3").value = downloadURL;
                            this.setState({
                                backgroundUrl3: downloadURL
                            });
                        });
                }));
            task4.on("state_changed",
                function progress(snapshot) {
                    console.log("task4", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task4.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-4").value = downloadURL;
                            this.setState({
                                backgroundUrl4: downloadURL
                            });
                        });
                }));
            task5.on("state_changed",
                function progress(snapshot) {
                    uploaderBackground.value = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                },
                function error (err) {
                    console.log("error", err);
                },
                (() => {
                    task5.snapshot.ref.getDownloadURL()
                        .then(downloadURL => {
                            document.querySelector(".background-option-5").value = downloadURL;
                            this.setState({
                                backgroundUrl5: downloadURL
                            });
                        });
                    modal.alert({
                        message: `All files has been uploaded`,
                        callback: function () {
                            console.log('does not have 5 files');
                        },
                        okTitle: 'OK'
                    });
                }));
        } else {
            modal.alert({
                message: `Please select 5 files at ones`,
                callback: function () {
                    console.log('does not have 5 files');
                },
                okTitle: 'OK'
            })
        }
    };

    correctAnswer = (e) => {
        let index = e.target.dataset.index;
        let value = document.querySelector(`.answer-option-${index}`).value || '';
        document.querySelector('.correctAnswer').value = value;
    };

    correctBackground = (e) => {
        let index = e.target.dataset.index;
        let value = document.querySelector(`.background-option-${index}`).value || '';
        document.querySelector('.correctAnswer').value = value;
    };

    render() {
        let topLeftImage = require("../../content/screenShots/Missing_item.png" || "");
        let topRightImage = require("../../content/screenShots/graph_sample.png" || "");
        let bottomLeftImage = require("../../content/Sample Questions.png" || "");
        let bottomRightImage = require("../../content/screenShots/background_options.png" || "");
        let imageUrl = this.props.url;
        return (
            <div className="adminPageTeam">
                <div className="questionsInputsTeam">
                    <form className="questionDiv">
                        <div className="topLine">
                            <input type="text" className="question-text" placeholder="question text" required/>
                            <input type="text" className="correctAnswer" placeholder="correct answer text" required/>
                            <input type="text" className="graph" placeholder="graph"/>
                            <img className="graphImageTeam" src={imageUrl} alt=""/>
                        </div>
                        <span data-index="1" onClick={this.correctAnswer}>CA1</span>
                        <input type="text" className="answer-option-1" name="options" placeholder="answer-option-1" />
                        <span data-index="2" onClick={this.correctAnswer}>CA2</span>
                        <input type="text" className="answer-option-2" name="options" placeholder="answer-option-2" />
                        <span data-index="3" onClick={this.correctAnswer}>CA3</span>
                        <input type="text" className="answer-option-3" name="options" placeholder="answer-option-3" />
                        <span data-index="4" onClick={this.correctAnswer}>CA4</span>
                        <input type="text" className="answer-option-4" name="options" placeholder="answer-option-4" />
                        <span data-index="5" onClick={this.correctAnswer}>CA5</span>
                        <input type="text" className="answer-option-5" name="options" placeholder="answer-option-5" /> <br/>

                        <span data-index="1" onClick={this.correctBackground}>CB1</span>
                        <input type="text" className="background-option-1" name="background" placeholder="backgroundImg-option-1" data-index="1"/>
                        <span data-index="2" onClick={this.correctBackground}>CB2</span>
                        <input type="text" className="background-option-2" name="background" placeholder="backgroundImg-option-2" data-index="2"/>
                        <span data-index="3" onClick={this.correctBackground}>CB3</span>
                        <input type="text" className="background-option-3" name="background" placeholder="backgroundImg-option-3" data-index="3"/>
                        <span data-index="4" onClick={this.correctBackground}>CB4</span>
                        <input type="text" className="background-option-4" name="background" placeholder="backgroundImg-option-4" data-index="4"/>
                        <span data-index="5" onClick={this.correctBackground}>CB5</span>
                        <input type="text" className="background-option-5" name="background" placeholder="backgroundImg-option-5" data-index="5"/><br/>

                        <button type="button" className="submitAdmin" onClick={this.pushNewQuestion}>Add Question</button>
                        <button type="button" className="clearAll" onClick={this.clearAll}>ClearAll</button>
                        <select type="text" className="selectGraphTeam" onChange={this.selectGraphDatabase}>
                            <option value="none">Choose graph</option>
                        </select>
                        <div className="upload-btn-wrapper">
                            <button className="btn">Upload graph</button>
                            <input type="file" id="fileButton"  onChange={this.uploadFile}/>
                            <progress value="0" max="100" id="uploader">0%</progress>
                        </div>
                        <div className="upload-btn-wrapper">
                            <button className="btn">Background</button>
                            <input type="file" id="fileButton" multiple  onChange={this.uploadBatch}/>
                            <progress value="0" max="100" id="uploaderBackground">0%</progress>
                        </div>
                        <a className="uploadLink" href="https://drive.google.com/drive/u/2/folders/17Zk8ToTHEhl_hirprcNxjRIPsWsvqNe6" target="_blank" rel="noopener noreferrer">Google drive</a>
                        <a className="uploadLink" href="https://www.canva.com/design/DAC7TGDsT1Q/share?role=EDITOR&token=DP3uV6IKzZaQH-iWyffHlQ&utm_content=DAC7TGDsT1Q&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton" target="_blank" rel="noopener noreferrer">Graph template</a>
                    </form>
                </div>
                <div className="topImages"><img className="topLeft" src={topLeftImage} alt=""/><img className="topRight" src={topRightImage} alt=""/></div>
                <div className="bottomImages"><img className="topLeft" src={bottomLeftImage} alt=""/><img className="topRight" src={bottomRightImage} alt=""/></div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    id: state.questionId.adminTeam,
    url: state.questionId.graphUrl,
    metadata: state.questionId.graphMetadata,
    allMetadata: state.questionId.allMetadata
});

const mapDispatchToProps = (dispatch) => ({
    newQuestionTeam: (questionObject) => dispatch(newQuestionTeam(questionObject)),
    graphUrl: (url) => dispatch(graphUrl(url)),
    selectGraphsList: (graphs) => dispatch(selectGraphsList(graphs)),
    graphMetadata: (metadata) => dispatch(graphMetadata(metadata)),
    graphName: (graph) => dispatch(graphName(graph)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AgaveTeam);