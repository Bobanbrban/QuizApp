import React from 'react';
import {connect} from "react-redux";
import modal from "modals";
import { deleteQuestion, startDeleteQuestion, graphUrl, changeId, graphName, graphMetadata, pathExist } from "../../actions/adminActions";


class QuestionElement extends React.Component {

    addToInput = (e) => {
        if(this.props.adminOrTeam === 'admin') {
            let index = e.target.dataset.index;
            let question = this.props.mainBoolean ? this.props.mainDatabase[index] : this.props.questions[index];
            document.querySelector('.question-textAdmin').value = question.text;
            document.querySelector('.correctAnswerAdmin').value = question.correctAnswer;
            document.querySelector('.graphAdmin').value = question.graph;
            document.querySelector('.graphUrlAdmin').value = typeof question.graphUrl !== "undefined" ? question.graphUrl : '';
            document.querySelector('.answer-option-1').value = question.answerOptions[1];
            document.querySelector('.answer-option-2').value = question.answerOptions[2];
            document.querySelector('.answer-option-3').value = question.answerOptions[3];
            document.querySelector('.answer-option-4').value = question.answerOptions[4];
            document.querySelector('.answer-option-5').value = question.answerOptions[5];
            document.querySelector('.background-option-1').value = typeof question.answerOptions.background !== "undefined" ? question.answerOptions.background[1] : '';
            document.querySelector('.background-option-2').value = typeof question.answerOptions.background !== "undefined" ? question.answerOptions.background[2] : '';
            document.querySelector('.background-option-3').value = typeof question.answerOptions.background !== "undefined" ? question.answerOptions.background[3] : '';
            document.querySelector('.background-option-4').value = typeof question.answerOptions.background !== "undefined" ? question.answerOptions.background[4] : '';
            document.querySelector('.background-option-5').value = typeof question.answerOptions.background !== "undefined" ? question.answerOptions.background[5] : '';
            this.props.graphUrl(question.graphUrl);
            this.props.graphName(question.graph);
            if (question.graphMetadata) {
                this.props.graphMetadata(question.graphMetadata);
            }
            if (this.props.mainBoolean) {
                document.querySelector('.question-group').value = question.questionGroup;
                document.querySelector('.points-value').value = question.points;
                this.props.changeId(question.questionId);
                if (question.path) {
                    this.props.pathExist(question.path)
                }
            }
        } else {
            modal.alert({
                message: `Ask admin for permission`,
                callback: function () {
                    console.log('Wrong password');
                },
                okTitle: 'OK'
            });
        }
    };


    confirmDelete = (e) => {
        e.persist();
        if(this.props.adminOrTeam === 'admin') {
            modal.confirm({
                message: 'Confirm delete question',
                callback: (val) => {
                    if(val === 'Ok') {
                        this.deleteQuestion(e);
                        console.log("test add admin",val)
                    }
                },
                okTitle: 'Ok',
                cancelTitle: 'Cancel'
            });
        } else {
            modal.alert({
                message: `Ask admin for permission`,
                callback: function () {
                    console.log('Wrong password');
                },
                okTitle: 'OK'
            });
        }
    };

    deleteQuestion = (e) => {
        let id = e.target.dataset.index;
        let question =  this.props.mainBoolean ? this.props.mainDatabase[id] : this.props.questions[id];
        let path = typeof question.path !== "undefined" ? question.path : '';
        let info = {id, path, main: this.props.mainBoolean};
        this.props.startDeleteQuestion(info);
    };

    render() {
        let question = this.props.question;
        let i = this.props.i;
        let style = {height: "40px"};
        return (
            <li style={style}>
                <span>{i + 1}</span>
                <span>{question.text}</span>
                <span>{question.correctAnswer}</span>
                <span>{question.graphShortName ? question.graphShortName : question.graph}</span>
                <span className="action">
                    <button onClick={this.addToInput} data-index={i} >ADD</button>
                    <button onClick={this.confirmDelete} data-index={i}>DELETE</button>
                </span>
            </li>
        )
    }
}

const mapStatetoProps = (state) => ({
    questions: state.questionId.questions,
    mainDatabase: state.questionId.questionsDatabase,
    mainBoolean: state.questionId.mainDatabase,
    adminOrTeam: state.questionId.adminOrTeam,
});

const mapDispatchToProps = (dispatch) => ({
    deleteQuestion: (id) => dispatch(deleteQuestion(id)),
    startDeleteQuestion: (info) =>  dispatch(startDeleteQuestion(info)),
    graphUrl: (url) => dispatch(graphUrl(url)),
    changeId: (id) => dispatch(changeId(id)),
    graphName: (graph) => dispatch(graphName(graph)),
    graphMetadata: (metadata) => dispatch(graphMetadata(metadata)),
    pathExist: (path) => dispatch(pathExist(path)),
});


export default connect( mapStatetoProps, mapDispatchToProps )(QuestionElement);

