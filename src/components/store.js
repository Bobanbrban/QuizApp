import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import questions from '../reducers/questions';
import userInfo from '../reducers/userInfo';
import answers from '../reducers/answers';
import totalPoints from '../reducers/totalPoints';
import questionId from '../reducers/adminReducer';
import infoText from '../reducers/infoText';
import secondStageRed from '../reducers/secondStageRed';
import progress from '../reducers/progressRed';
import candidates from '../reducers/candidatesReducer';
import pages from '../reducers/activePages';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const composeEnhancers = compose;

export default () => {
    const store = createStore(
        combineReducers({
            questions: questions,
            order: userInfo,
            answers: answers,
            total: totalPoints,
            questionId: questionId,
            infoText: infoText,
            secondStage: secondStageRed,
            progress: progress,
            candidates: candidates,
            pages: pages,
        }),
        composeEnhancers(applyMiddleware(thunk))
    );
    return store;
};
