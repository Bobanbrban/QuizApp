import React from 'react';

const LoadingGif = (props) => (
    <div className="loadingGifPage">
        <img className="cubeLoaderGif" src={props.image} alt=""/>
    </div>
);

export default LoadingGif;
