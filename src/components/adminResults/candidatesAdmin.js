import React from 'react';
import { connect } from 'react-redux';
import AdminOnly from "../adminOnly";
import CandidatesList from "./candidatesList";
import AllAnswersDiv from "./allAnswersDiv";
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import db from "../../firebase/firebase";
import {loadCandidates, filteredCandidates} from "../../actions/candidatesActions";

import {correctPassword} from "../../actions/adminActions";

class CandidatesAdmin extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            startDate: moment(),
            dateChanged: false,
            openedAnswers: false
        };
        this.handleChange = this.handleChange.bind(this);
    };

    componentWillMount = () => {
        let loadCandidates = () => {
            let candidatesArray = [];
            let userRef = db.collection(`users`).orderBy("total", "desc");
            userRef.get()
                .then(async doc => {
                    doc.forEach(doc => {
                        candidatesArray.push(doc.data());
                    });
                    let filteredCandidates = await candidatesArray.filter(cand => typeof cand.user !== "undefined" && typeof cand.user.displayName !== "undefined");
                    this.props.loadCandidates(filteredCandidates);
                })
        };
        loadCandidates();
    };

    findMatches = (wordToMatch, names, key) => {
        if(key !== 220) {
            return names.filter(name => {
                // here we need to figure out if the city or state matches what was searched
                let wordToMatchSmall = wordToMatch.toLowerCase();
                const regex = new RegExp(wordToMatchSmall, 'gi');
                let fullNameSmall = name.fullName.toLowerCase();
                let emailSmall = name.eMail.toLowerCase();
                return fullNameSmall.match(regex) || emailSmall.match(regex);
            });
        }
    };
    displayMatches = () => {
            let searchInput = document.querySelector('.searchCandidateInput').value;
            if (searchInput.slice(-1) !== "\\") {
            let names = [];
            this.props.candidates.map(cand => {
                let eMail = cand.user.email;
                let fullName = {eMail: eMail, fullName: cand.user.displayName, curp: cand.candidateInfo.curp};
                return names.push(fullName);
            });
            let matched = this.findMatches(searchInput, names);
            let filteredArray = [];
              for (let i = 0; i < this.props.candidates.length; i++) {
                  let curp = this.props.candidates[i].candidateInfo.curp;
                      matched.map(data => data.curp === curp ? filteredArray.push(this.props.candidates[i]) : console.log(''));
              }

            this.props.filteredCandidates(filteredArray);
            let displayHlSpan = () => {
                let searchInput = document.querySelector('.searchCandidateInput').value;
                let searchInputCapital = typeof searchInput[0] !== "undefined" ? searchInput[0].toUpperCase() + searchInput.substring(1) : '';
                let candName = document.querySelectorAll('.candidateName');
                let filteredArray = this.props.filteredState;
                let i;
                for (i = 0; i < filteredArray.length; i++) {
                    const regex = new RegExp(searchInputCapital, 'gi');
                    let nameLower = filteredArray[i].user.displayName.toLowerCase();
                    let inputLower = searchInput.toLowerCase();
                    let firstLetter = nameLower[0] === inputLower[0] ? true : false;
                    const fullName = filteredArray[i].user.displayName.replace(regex, `<span class="spanHl">${firstLetter === true ? searchInputCapital : searchInput}</span>`);
                    let html = `<span class="name">${fullName}</span>`;
                    candName[i].innerHTML = html;
                }
            };
            setTimeout(displayHlSpan, 0);
        }
    };

    filterFirst = () => {
        let userRef = this.props.filteredState.sort((a,b) => b.total - a.total);
                    this.props.filteredCandidates(userRef);
    };
    filterSecond = () => {
        let userRef = this.props.candidates;
           userRef = userRef.filter(cand => cand.total > 59);
                this.props.filteredCandidates(userRef);
    };
    filterSecondPoints = () => {
        let userRef = this.props.filteredState.sort((a,b) => b.secondStepResult - a.secondStepResult);
        this.props.filteredCandidates(userRef);
    };
    filterEmail = () => {
        let userRef = this.props.filteredState.sort((a,b) => a.user.email.toLowerCase().localeCompare(b.user.email.toLowerCase()));
        this.props.filteredCandidates(userRef);
    };
    filterInterview = () => {
        let userRef = this.props.candidates;
           userRef = userRef.filter(cand => cand.invitedForInterview === true);
                this.props.filteredCandidates(userRef);
    };
    filterDate = () => {
        let userRef = this.props.filteredState.sort((a,b) => b.timestamp - a.timestamp);
        this.props.filteredCandidates(userRef);
    };

    filterInterviewDate = () => {
        let userRef = this.props.filteredState.sort((a,b) => b.interviewTimestamp - a.interviewTimestamp);
        this.props.filteredCandidates(userRef);
    };

    filterFirstName = () => {
        let userRef = this.props.filteredState.sort((a,b) => a.user.given_name && b.user.given_name ?  a.user.given_name.toLowerCase().localeCompare(b.user.given_name.toLowerCase()) : null);
        this.props.filteredCandidates(userRef);
    };
    filterLastName = () => {
        let userRef = this.props.filteredState.sort((a,b) => a.user.family_name && b.user.family_name ? a.user.family_name.toLowerCase().localeCompare(b.user.family_name.toLowerCase()) : null);
        this.props.filteredCandidates(userRef);
    };

    candidatesFilter = () =>{
        let selectFilterValue = document.querySelector(".selectFilter").value;
        switch(selectFilterValue) {
            case "first":
                this.filterFirst();
                break;
            case "second":
                this.filterSecondPoints();
                break;
            case "firstName":
                this.filterFirstName();
                break;
            case "lastName":
                this.filterLastName();
                break;
            case "date":
                this.filterDate();
                break;
            case "interviewDate":
                this.filterInterviewDate();
                break;
            case "email":
                this.filterEmail();
                break;
            default:
        };
    };

    loadAllCandidates = () => {
           let allData = this.props.candidates;
                this.props.filteredCandidates(allData);
    };

    candidatesFilterInterview = () =>{
        let selectFilterValue = document.querySelector(".selectFilterInterview").value;
        switch(selectFilterValue) {
            case "interview":
                this.filterInterview();
                break;
            case "all":
                this.loadAllCandidates();
                break;
            case "second":
                this.filterSecond();
                break;
            default:
        };
    };

    handleChange(date) {
        let interviewTimestamp = date.format('x');
        let formatedYear = date.format('MMMM Do YYYY');
        let formatedTime = date.format('h:mm a');
        let dateString = formatedYear  + " at " + formatedTime;
        this.setState({
            startDate: date,
            dateChanged: true,
            dateString: dateString,
            interviewTimestamp
        });
    }

    adminPassword = (e) => {
        e.preventDefault();
        let password = document.querySelector('.adminPasswordInput').value;
        if(password === 'agave123lab' || password === 'slagalica1') {
            this.props.correctPassword('agaveTeam');
        }
    };

    openAnswers = () => {
        this.setState({openedAnswers: !this.state.openedAnswers});
    };

    render () {
        let average = 0;
        let qualifiedForSecond = 0;
        let filtered = this.props.candidates.filter(cand => typeof cand.user.displayName !== "undefined");
        filtered.map(cand => {
            average = average + cand.total;
            return cand.secondStepQualified === true ?  qualifiedForSecond++ : qualifiedForSecond;
        });
        // eslint-disable-next-line
        let AverageTotal = Math.floor(average/filtered.length) || 0;
            return (
            <div className="candidatesResultsPage">
                {!this.props.adminTrue && <AdminOnly adminPassword={this.adminPassword} />}
                {this.state.openedAnswers && <AllAnswersDiv openAnswers={this.openAnswers} />}
                <div className="generalInfo">
                    <span className="numberOfCandidates">
                        <div>
                            <div>FIRST</div>
                            <div>{filtered.length}</div>
                        </div>
                    </span>
                    <span className="numberOfCandidates">
                         <div>
                            <div>SECOND</div>
                            <div>{qualifiedForSecond}</div>
                        </div>
                    </span>
                    <span className="averagePoints">
                          <div>
                            <div>AVERAGE</div>
                            <div>{AverageTotal}</div>
                        </div>
                    </span>
                    <select className="selectFilterInterview" onChange={this.candidatesFilterInterview}>
                        <option value="all">All candidates</option>
                        <option value="interview">Invited for interview</option>
                        <option value="second">Qualified for second</option>
                    </select>
                    <select className="selectFilter" onChange={this.candidatesFilter}>
                        <option value="first">First stage points</option>
                        <option value="second">Second stage points</option>
                        <option value="date">Sort by date</option>
                        <option value="interviewDate">Sort by interview date</option>
                        <option value="firstName">Sort by first name</option>
                        <option value="lastName">Sort by last name</option>
                        <option value="email">Sort by email</option>
                    </select>
                    <DatePicker
                        selected={this.state.startDate}
                        onChange={this.handleChange}
                        showTimeSelect
                        minTime={moment().hours(9).minutes(0)}
                        maxTime={moment().hours(17).minutes(0)}
                        dateFormat="LLL"
                    />
                    <input className="searchCandidateInput" onKeyUp={this.displayMatches} placeholder="search all"/>
                </div>
                <CandidatesList date={this.state} openAnswers={this.openAnswers}/>
                <div className="background">
                </div>
                <div className="backgroundLinear">
                </div>
            </div>
        )
    }
};

const mapStateToProps = (state) => ({
    candidates: state.candidates.onLoadState,
    filteredState: state.candidates.filteredState,
    adminTrue: state.questionId.adminPassword,
});

const mapDispatchToProps = (dispatch) => ({
    loadCandidates: (candidatesArray) => dispatch(loadCandidates(candidatesArray)),
    filteredCandidates: (filteredData) => dispatch(filteredCandidates(filteredData)),
    correctPassword: (text) => dispatch(correctPassword(text)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CandidatesAdmin);
