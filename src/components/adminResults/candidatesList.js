import React from 'react';
import { connect } from 'react-redux';
import CandidatesElement from "./candidatesElement";

class CandidatesList extends React.Component {

    render() {
        let list;
        let candidatesArray = this.props.filteredCandidates;
        list = candidatesArray.map((candidate, index) => {
            return (
                <CandidatesElement element={candidate}
                                   key={index}
                                   index={index}
                                   allCandidates={candidatesArray}
                                   date={this.props.date}
                                   adminState={this.props.state}
                                   openAnswers={this.props.openAnswers}
                />
            )
        });
        return (
                <ul className="candidatesList">{list}</ul>
        )
    }
}

const mapStateToProps = (state) => ({
    candidates: state.candidates.onLoadState,
    filteredCandidates: state.candidates.filteredState
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(CandidatesList);
