import React from 'react';
import { connect } from 'react-redux';
import AllAnswersList from './allAnswersList';


class AllAnswersDiv extends React.Component {

    render() {
        return (
            <div className="allAnswersPage">
                <AllAnswersList />
            </div>
        )
    }
}


const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});


export default connect(mapStateToProps, mapDispatchToProps)(AllAnswersDiv);