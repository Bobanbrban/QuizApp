import React from 'react';
import modal from "modals";
import db from '../../firebase/firebase';
import dbClone from "../../firebase/firebaseClone";
import { openDiv, addCommentLocally, invitedForInterview } from "../../actions/candidatesActions";
import { allAnswersResults } from "../../actions/actions";
import  { connect } from "react-redux";

class CandidatesElement extends React.Component {
    addInterviewMark = (e) => {
        let index = e.target.dataset.index;
        if(typeof this.props.allCandidates[index] !== "undefined") {
            let curp = this.props.allCandidates[index].candidateInfo.curp;
            console.log(curp);
            let element = this.props.element;
            let name = element.user.displayName;
            let email = element.user.email;
            let date = this.props.date;
            let interviewDate = date.dateString;
            let interviewTimestamp = date.interviewTimestamp;
            if (date.dateChanged) {
                let interviewDateDataBase = interviewDate || '';
                let indexObj = {index, interviewDateDataBase, interviewTimestamp};
                this.props.invitedForInterview(indexObj);

                const userRefInterview = db.doc(`interview/${curp}`);
                userRefInterview.set({name, email, interviewDate});

                const userRef = db.doc(`users/${curp}`);
                const userRefClone = dbClone.doc(`users/${curp}`);
                userRef.get()
                    .then(doc => {
                    if(doc.exists) {
                        userRef.update({
                            invitedForInterview: true,
                            interviewDate,
                            interviewTimestamp
                        });
                        userRefClone.update({
                            invitedForInterview: true,
                            interviewDate,
                            interviewTimestamp
                        });
                    } else {
                        console.log("doc with that curp number doesn't exist");
                    }
                });


                userRefInterview.delete()
                    .then(() => console.log("Document successfully deleted!"))
                    .catch(error => console.error("Error removing document: ", error));

            } else {
                modal.alert({
                    message: `Select interview date`,
                    callback: function () {
                        console.log('Select interview date');
                    },
                    okTitle: 'OK'
                });
            }
        }
    };


addComment = (e) => {
        let index = e.target.dataset.index;
        let commentText = document.querySelectorAll(".ourCommentInput");
        let thisComment = commentText[index].value;
        let info = {thisComment,index};
        if (thisComment !== "") {
            this.props.addCommentLocally(info);
            let candidateCurp = this.props.filteredCandidates[index].candidateInfo.curp;
            const userRef = db.doc(`users/${candidateCurp}`);
            const userRefClone = dbClone.doc(`users/${candidateCurp}`);
            userRef.update({ourComment:thisComment});
            userRefClone.update({ourComment:thisComment});
            let allInputs = document.querySelectorAll(".ourCommentInput");
            allInputs[index].value = '';
        }
    };

    openDivOnClick = (e) => {
        let index = e.target.dataset.index;
        if(index && e.target.classList[0] !== "invitedForInterview-span__interview") {
            this.props.openDiv(index);
        }
    };

    openAllAnswers = () => {
        let element = this.props.element;
        let answers = element.allAnswers;
        console.log(answers);
        this.props.allAnswersResults(answers);
        this.props.openAnswers();

    };



render() {
    let element = this.props.element;
    let name = element.user.displayName;
    let created = element.createdAt;
    let total = element.total;
    let image = element.user.photoURL;
    let email = element.user.email;
    let href = `mailto:${email}`;
    let secondResult = element.secondStepResult || 0;
    let secondTime = element.secondStepTime || "0:00:00";
    let invited = element.invitedForInterview;
    let interviewDate = element.interviewDate;
    let riddleResult = typeof element.riddleObject !== "undefined" ?  element.riddleObject.riddleResult : 0;
    let riddleMin = typeof element.riddleObject  !== "undefined" ? element.riddleObject.riddleMin : 0;
    let riddleSec = typeof element.riddleObject  !== "undefined" ? element.riddleObject.riddleSec : 0;
    let riddleHour = typeof element.riddleObject  !== "undefined" ? element.riddleObject.riddleHour : 0;
    let question2Min = typeof element.question2answer  !== "undefined" ? element.question2answer.question2min : 0;
    let question2Hour = typeof element.question2answer !== "undefined" ? " " + element.question2answer.question2hour : 0;
    let question2Sec = typeof element.question2answer !== "undefined" ? element.question2answer.question2sec : 0;
    let question = typeof element.questionFromCandidate !== "undefined" ? element.questionFromCandidate.question : '';
    let option1 = typeof element.questionFromCandidate !== "undefined" ? element.questionFromCandidate.option1 : '';
    let option2 = typeof element.questionFromCandidate !== "undefined" ? element.questionFromCandidate.option2 : '';
    let option3 = typeof element.questionFromCandidate !== "undefined" ? element.questionFromCandidate.option3 : '';
    let option4 = typeof element.questionFromCandidate !== "undefined" ? element.questionFromCandidate.option4 : '';
    let option5 = typeof element.questionFromCandidate !== "undefined" ? element.questionFromCandidate.option5 : '';
    let correct = typeof element.questionFromCandidate !== "undefined" ? element.questionFromCandidate.correct : '';
    let infoCity = typeof element.candidateInfo !== "undefined" ? element.candidateInfo.input1City : '';
    let infoAge = typeof element.candidateInfo !== "undefined" ? element.candidateInfo.input2Age : '';
    let infoEducation = typeof element.candidateInfo !== "undefined" ? element.candidateInfo.input3Education : '';
    let infoDirection = typeof element.candidateInfo !== "undefined" ? element.candidateInfo.sellectInterest : '';
    let infoJob = typeof element.candidateInfo !== "undefined" ? element.candidateInfo.input4Job : '';
    let infoCurp = typeof element.candidateInfo !== "undefined" ? element.candidateInfo.curp : '';
    let styleRed = {
        color: "#FC5652",
    };
    let styleGreen = {
        color: "#00B3A0",
    };
    let classNameCollapsed = `hidden collapsedDiv${this.props.index}`;
    let hidden = {
        display: "none"
    };
    let openDiv = {
        display: "flex"
    };

    let isHidden = element.isHidden;
    let question2Full = typeof  element.question2answer !== "undefined" ? element.question2answer.fullAnswerTwo : '';
    let question2Final = typeof  element.question2answer !== "undefined" ? element.question2answer.finalAnswerTwo : '';
    let question2FinalCorrect = question2Final === "3" ? "correct" : "not correct ";
    let comment = element.ourComment;

    return (
        <div className="candidateDiv" data-index={this.props.index}>
            <li className='candidatesElement' data-index={this.props.index} onClick={this.openDivOnClick}>
                <img className="candidateImageImg" src={image} alt=""/>
                <span className="candidateName">{name}</span>
                <span className="candidateTotal" style={total < 60 ? styleRed : styleGreen}>{total}</span>
                <span className="secondStageInfo">
                    <span className="secondStageResult"
                          style={secondResult < 1 ? styleRed : styleGreen}>{secondResult}</span>
                    <span className="secondStageTime"
                          style={secondTime === "0:00:00" ? styleRed : styleGreen}>{secondTime}</span>
                </span>
                <span className="candidateDate">{created}</span>
                <span className="candidateEmail"><a href={href}>{email}</a></span>
                <span className="invitedForInterview-span" onClick={this.addInterviewMark}>
                    <div className="invitedForInterview-span__interview" data-index={this.props.index} onClick={this.addInterviewMark} style={invited === true ? styleGreen : {}}>Interview</div>
                    <div className="invitedForInterview-span__date" >{interviewDate ? interviewDate : "Not invited yet"}</div>
                </span>
            </li>
            <div className="allHidden" style={isHidden === true ? hidden : openDiv}>
                <div className={classNameCollapsed}>
                    <div className="candidatesInfo">
                        <span>{infoCity}</span>
                        <span>{infoAge}</span>
                        <span>{infoDirection}</span>
                        <span>{infoEducation}</span>
                        <span>{infoJob}</span>
                        <span className="infoCurp">{infoCurp}</span>
                        <div type="button" className="allAnswersResults" data-index={this.props.index}
                                onClick={this.openAllAnswers}>All answers
                        </div>
                    </div>
                    <div className="secondStageAnswers">
                        <div className="secondStageAnswersTop">
                            <div className="riddleAnswer">
                                <div className="riddleAnswerText"> Riddle
                                    is {riddleResult === 1 ? "correct " : "not correct"}</div>
                                <div className="riddleTime">{riddleHour + ":" + riddleMin + ":" + riddleSec}</div>
                            </div>
                            <div className="secondAnswer">
                                <div className="secondAnswerFinall">Second is {question2FinalCorrect}</div>
                                <div className="question2Time"> {question2Hour + ":" + question2Min + ":" + question2Sec}</div>
                            </div>
                        </div>
                        <div className="secondAnswerFull">{question2Full ? question2Full : ""}</div>
                    </div>
                    <div className="ourComment">
                        <div className="commentDiv">
                            <input className="ourCommentInput" placeholder="our comment" />
                            <button type="button" className="submitOurComment" data-index={this.props.index}
                                    onClick={this.addComment}>Add
                            </button>
                            <div className="commentText">{comment}</div>
                        </div>
                    </div>
                </div>
                <div className="candidateQuestion">
                    <span className="QuestionCandidate">{question !== "" ? "question: " + question : ''}</span>
                    <span className="option1candidate">{option1 !== "" ? "1: " + option1 : ''}</span>
                    <span className="option1candidate">{option2 !== "" ? "2: " + option2 : ''}</span>
                    <span className="option1candidate">{option3 !== "" ? "3: " + option3 : ''}</span>
                    <span className="option1candidate">{option4 !== "" ? "4: " + option4 : ''}</span>
                    <span className="option1candidate">{option5 !== "" ? "5: " + option5 : ''}</span>
                    <span className="correctCandidate">{correct !== "" ? "correct: " + correct : ''}</span>
                </div>
                <div className="commentFromCandidate">{element.comment ? element.comment : ""}</div>
            </div>
        </div>
    )
}
};

const mapStateToProps = (state) => ({
    candidates: state.candidates.onLoadState,
    filteredCandidates: state.candidates.filteredState
});

const mapDispatchToProps = (dispatch) => ({
    openDiv : (index) => dispatch(openDiv(index)),
    addCommentLocally : (info) => dispatch(addCommentLocally(info)),
    invitedForInterview: (indexObj) => dispatch(invitedForInterview(indexObj)),
    allAnswersResults: (answers) => dispatch(allAnswersResults(answers)),

});

export default connect(mapStateToProps, mapDispatchToProps)(CandidatesElement);
