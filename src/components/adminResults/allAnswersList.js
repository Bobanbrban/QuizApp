import React from 'react';
import { connect } from 'react-redux';
import AllAnswersElements from "./allAnswersElements";


class AllAnswersList extends React.Component {

    render() {

        let list;
        let allAnswersArray = this.props.allAnswers;
        list = allAnswersArray.map((answer, index) => {
            return (
                <AllAnswersElements answer={answer}
                                    key={index}
                                    index={index}
                                    openAnswers={this.props.openAnswers}
                />
            )
        });
        return (
            <ul className="allAnswersResultList">{list}</ul>
        )
    }
}


const mapStateToProps = (state) => ({
    allAnswers: state.answers
});

const mapDispatchToProps = (dispatch) => ({

});


export default connect(mapStateToProps, mapDispatchToProps)(AllAnswersList);