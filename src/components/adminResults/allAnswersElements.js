import React from 'react';
import { connect } from 'react-redux';


class AllAnswersElements extends React.Component {

    render() {
        let answer = this.props.answer;
        console.log(answer);
        let question = answer.question;
        return (
            <div className="allAnswersElements">
                <div className="allAnswersElements-question">{question}</div>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});


export default connect(mapStateToProps, mapDispatchToProps)(AllAnswersElements);