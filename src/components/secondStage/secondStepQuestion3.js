import React from 'react';
import db from "../../firebase/firebase";
import dbClone from "../../firebase/firebaseClone";
import moment from "moment";

class SecondStepQuestion3 extends React.Component {

    savingQuestionTree = () => {
        let fullAnswerTwo = JSON.parse(localStorage.getItem("question2FullAnswer"));
        let finalAnswerTwo = JSON.parse(localStorage.getItem("question2FinalAnswer"));
        let question2min = JSON.parse(localStorage.getItem("question2Min"));
        question2min = question2min < 10 ? "0" + question2min : question2min;
        let question2sec = JSON.parse(localStorage.getItem("question2Sec"));
        question2sec = question2sec < 10 ? "0" + question2sec : question2sec;
        let question2hour = JSON.parse(localStorage.getItem("question2Hour"));
        let resultQuestionTwo = JSON.parse(localStorage.getItem("question2Result"));

        let question = document.querySelector(".questionTextArea").value;
        let option1 = document.querySelector(".answerOption1Candidate").value;
        let option2 = document.querySelector(".answerOption2Candidate").value;
        let option3 = document.querySelector(".answerOption3Candidate").value;
        let option4 = document.querySelector(".answerOption4Candidate").value;
        let option5 = document.querySelector(".answerOption5Candidate").value;
        let correct = document.querySelector(".correctCandidate").value;
        let curp = JSON.parse(localStorage.getItem("curp"));
        let riddleMin = JSON.parse(localStorage.getItem("riddleMin"));
        riddleMin = riddleMin < 10 ? "0" + riddleMin : riddleMin;
        let riddleSec = JSON.parse(localStorage.getItem("riddleSec"));
        riddleSec = riddleSec < 10 ? "0" + riddleSec : riddleSec;
        let riddleHour = JSON.parse(localStorage.getItem("riddleHour"));
        let riddleResult = JSON.parse(localStorage.getItem("riddleResult"));
        let question2answer = {
            fullAnswerTwo, finalAnswerTwo, question2min, question2sec, question2hour, resultQuestionTwo
        };

        let riddleObject = {
            riddleResult , riddleMin, riddleSec, riddleHour
        };
        let questionFromCandidate = {
            question, option1, option2, option3, option4, option5, correct
        };

        let secondStepResult = riddleResult + resultQuestionTwo;

        let minTotal = localStorage.getItem("totalMin");
        let secTotal = localStorage.getItem("totalSec");
        let hourTotal = localStorage.getItem("totalHour");
        let min = minTotal < 10 ? "0" + minTotal : minTotal;
        let sec = secTotal < 10 ? "0" + secTotal : secTotal;
        // eslint-disable-next-line
        let secondStepTime = `${hourTotal}`+ ":" + `${min}`+ ":" + `${sec}`;
        let secondCompletedAt = moment().format('MMMM Do YYYY');
        let secondStepTimestamp = new Date().getTime();

        let allAnswers = {
           question2answer, riddleObject, questionFromCandidate, secondStepCompleted: true, secondStepResult, secondStepTime, secondCompletedAt, secondStepTimestamp
        };

        let SecondSetpInfo = JSON.parse(JSON.stringify(allAnswers));
        let docRef = db.doc(`users/${curp}`);
        let docRefClone = dbClone.doc(`users/${curp}`);

        docRef.update(SecondSetpInfo);
        docRefClone.update(SecondSetpInfo);
    };

deleteAllLocalStorage = () => {
    localStorage.removeItem("question2FullAnswer");
    localStorage.removeItem("question2FinalAnswer");
    localStorage.removeItem("question2Min");
    localStorage.removeItem("question2Sec");
    localStorage.removeItem("question2Hour");
    localStorage.removeItem("question2Result");
    localStorage.removeItem("riddleMin");
    localStorage.removeItem("riddleSec");
    localStorage.removeItem("riddleHour");
    localStorage.removeItem("riddleResult");
    localStorage.removeItem("riddleStarted");
    localStorage.removeItem("question2Started");
};

    goToFinalPage = () => {
        this.savingQuestionTree();
        setTimeout(this.deleteAllLocalStorage(),100);
        setTimeout(this.props.finalPageFunction(),200);
};
    render() {
        return (
            <div className="question3Page">
                <div className="questionsForQuiz">What would be the good question for Agave Quiz in your opinion ? Write one question with 5 answer options.</div>
                <textarea type="text" className="questionTextArea" />
                <div className="answerOptionsCandidats">
                    <span className="options" />
                    <input type="text" className="answerOption1Candidate" required placeholder="answer option 1" />
                    <input type="text" className="answerOption2Candidate" required placeholder="answer option 2" />
                    <input type="text" className="answerOption3Candidate" required placeholder="answer option 3" />
                    <input type="text" className="answerOption4Candidate" required placeholder="answer option 4" />
                    <input type="text" className="answerOption5Candidate" required placeholder="answer option 5" />
                    <input type="text" className="correctCandidate" required placeholder="correct answer" />
                    <button className="submitQuestionsBuckets" onClick={this.goToFinalPage}>Submit all</button>
                </div>
            </div>
        )
    }
}


export default SecondStepQuestion3;