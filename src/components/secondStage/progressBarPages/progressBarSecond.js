import React from 'react';
import LogoTopBar from "./logoTopBar";

class ProgressQualifiedForSecond extends React.Component {

                render() {
                    let total = this.props.state.total || JSON.parse(localStorage.getItem("total"));
                    let totalPossible = JSON.parse(localStorage.getItem("totalPossible")) || 100;
                    let completed = this.props.state.secondCompleted;
                    let quizCompletedAt = this.props.state.quizCreatedAt;
                    let completedAt = this.props.state.secondCompletedAt;
                    let secondResult = this.props.state.secondResult;
                    let secondTime = this.props.state.secondTime;
                    let arrow = require("../../../content/icons/arrow-angle-pointing-to-right.svg");
                    return (
                        <div className="progressPage">
                            <LogoTopBar/>
                            <div className="progressPageDivs">
                                <div className="stageOneProgressSecond" >
                                    <section>
                                        <div>Step One</div>
                                        <span>Quiz</span>
                                    </section>
                                    <div className="resultDivSecond">
                                        <span className="totalResultSpanSecond">{total}/{totalPossible}</span>
                                    </div>
                                    <div className="quizCompletedDate">{quizCompletedAt}</div>
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageTwoProgressSecond" >
                                    <div className="linearBackground"></div>
                                    <section>
                                        <div>Step Two</div>
                                        <span>Logic Test</span>
                                    </section>
                                    { !completed &&
                                        <button onClick={this.props.riddleTrue}>Start</button>
                                    }
                                    { completed &&
                                    <div className="secondCompletedDiv">
                                        <div className="secondCompletedText">{completedAt}</div>
                                        <div className="secondCompletedResult">{secondResult} correct in {secondTime}</div>
                                    </div>
                                    }
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageTreeProgressSecond">
                                    <section>
                                        <div>Step Tree</div>
                                        <span>Interview</span>
                                    </section>
                                    <p>Not qualified yet</p>
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageFourProgressSecond">
                                    <section>
                                        <div>Step Four</div>
                                        <span>Training</span>
                                    </section>
                                    <p>Not qualified yet</p>
                                </div>
                            </div>
                        </div>
                    )
                }
            }

        export default ProgressQualifiedForSecond;
