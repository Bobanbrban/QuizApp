import React from "react";

class LogoTopBar extends React.Component {

    render() {
        let imagePng = require("../../../content/DF-logo.png");
        return (
            <div className="progressLogo">
                <img src={imagePng} className="devFactoryLogo" alt=""/>
            </div>
        )
    }
};


export default LogoTopBar;