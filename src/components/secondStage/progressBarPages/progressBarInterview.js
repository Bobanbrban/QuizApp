import React from 'react';
import LogoTopBar from "./logoTopBar";

class ProgressQualifiedForInterview extends React.Component {

    render() {
        let total = this.props.state.total || JSON.parse(localStorage.getItem("total"));
        let totalPossible = JSON.parse(localStorage.getItem("totalPossible")) || 100;
        let quizCompletedAt = this.props.state.quizCreatedAt;
        let completedAt = this.props.state.secondCompletedAt;
        let intervieDate = this.props.state.interviewDate;
        let secondResult = this.props.state.secondResult;
        let secondTime = this.props.state.secondTime;
        let arrow = require("../../../content/icons/arrow-angle-pointing-to-right.svg");
        return (
            <div className="progressPage">
                <LogoTopBar/>
                <div className="progressPageDivs">
                    <div className="stageOneProgressInterview">
                            <section>
                                <div>Step One</div>
                                <span>Quiz</span>
                            </section>
                        <div className="resultDivInterview">
                            <span className="totalResultSpanInterview">{total}/{totalPossible}</span>
                        </div>
                        <div className="quizCompletedDate">{quizCompletedAt}</div>
                    </div>
                    <img src={arrow} alt=""/>
                    <div className="stageTwoProgressInterview">
                            <section>
                                <div>Step Two</div>
                                <span>Logic test</span>
                            </section>
                             <div className="secondCompletedText">{completedAt}</div>
                            <div className="secondCompletedResult">{secondResult} correct in {secondTime}</div>
                    </div>
                    <img src={arrow} alt=""/>
                    <div className="stageTreeProgressInterview">
                        <div className="linearBackground"/>
                            <section>
                                <div>Step Tree</div>
                                <span>Interview</span>
                            </section>
                        <p>{intervieDate ? intervieDate : "Not qualified yet"}</p>
                    </div>
                    <img src={arrow} alt=""/>
                    <div className="stageFourProgressInterview">
                            <section>
                                <div>Step Four</div>
                                <span>Training</span>
                            </section>
                        <p>Not qualified yet</p>
                    </div>
                </div>
            </div>
        )
    }
}


export default ProgressQualifiedForInterview;