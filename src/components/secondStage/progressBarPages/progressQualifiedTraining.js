import React from 'react';
import LogoTopBar from "./logoTopBar";

class ProgressQualifiedForTraining extends React.Component {

                render() {
                    let total = this.props.state.total || JSON.parse(localStorage.getItem("total"));
                    let totalPossible = JSON.parse(localStorage.getItem("totalPossible")) || 100;
                    let quizCompletedAt = this.props.state.quizCreatedAt;
                    let completedAt = this.props.state.secondCompletedAt;
                    let intervieDate = this.props.state.interviewDate;
                    let secondResult = this.props.state.secondResult;
                    let secondTime = this.props.state.secondTime;
                    let arrow = require("../../../content/icons/arrow-angle-pointing-to-right.svg");
                    return (
                        <div className="progressPage">
                            <LogoTopBar/>
                            <div className="progressPageDivs">
                                <div className="stageOneProgressTraining" >
                                    <section>
                                        <div>Step One</div>
                                        <span>Quiz</span>
                                    </section>
                                    <div className="resultDivTraining">
                                        <span className="totalResultSpanTraining">{total}/{totalPossible}</span>
                                        <span className="checkedSpanTraining"></span>
                                    </div>
                                    <div className="quizCompletedDate">{quizCompletedAt}</div>
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageTwoProgressTraining">
                                    <section>
                                        <div>Step Two</div>
                                        <span>Logic Test</span>
                                    </section>
                                    <div className="secondCompletedText">{completedAt}</div>
                                    <div className="secondCompletedResult">{secondResult} correct in {secondTime}</div>
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageTreeProgressTraining">
                                    <section>
                                        <div>Step Tree</div>
                                        <span>Interview</span>
                                    </section>
                                    <p>{intervieDate ? intervieDate : "Unknown date"}</p>
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageFourProgressTraining">
                                    <div className="linearBackground"></div>
                                    <section>
                                        <div>Step Four</div>
                                        <span>Training</span>
                                    </section>
                                    <p>Not qualified yet</p>
                                </div>
                            </div>
                        </div>
                    )
                }
            }


        export default ProgressQualifiedForTraining;

