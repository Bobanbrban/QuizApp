import React from 'react';
import connect from "react-redux/es/connect/connect";
import db from "../../../firebase/firebase";
import LogoTopBar from "./logoTopBar";
import {clearLocalStorage} from "../../../actions/actions";

class ProgressQuiz extends React.Component {
                    constructor(props){
                        super(props);
                        this.state = {
                            firstTimeInfo: false,
                            moreThenTwo: false,
                            curp: this.props.state.curp,
                        }
                    }

                    componentDidMount() {
                        this.props.clearLocalStorage();
                        let curp = this.props.state.curp;
                        let docRef = db.doc(`users/${curp}`);
                        docRef.get()
                            .then(doc => {
                                if(new Date().getTime() - doc.data().valueOf().timestamp > 5184000000){
                                    this.setState({moreThenTwo: true})
                                }
                                if(!doc.data().valueOf().firstTimeInfo) {
                                    this.setState({firstTimeInfo: true})
                                }
                            })
                    }

                    clearFirstTime = () => {
                        let docRef = db.doc(`users/${this.state.curp}`);
                        docRef.update({firstTimeInfo: true});
                        this.setState({firstTimeInfo: false});
                    };

                    afterTwoMonths = () => {
                        let curpUpper = this.state.curp;
                        this.props.getQuestions(curpUpper,undefined);
                    };

                render() {
                    let total = this.props.state.total || JSON.parse(localStorage.getItem("total"));
                    let totalPossible = JSON.parse(localStorage.getItem("totalPossible")) || 100;
                    let quizCompletedAt = this.props.state.quizCreatedAt;
                    let arrow = require("../../../content/icons/arrow-angle-pointing-to-right.svg");
                    return (
                        <div className="progressPage">
                            <LogoTopBar/>
                            <div className="progressPageDivs">
                                <div className="stageOneProgressQuiz">
                                    <section>
                                        <div>Step One</div>
                                        <span>Quiz</span>
                                    </section>
                                    {!this.state.moreThenTwo &&
                                    <div className="resultsDiv">
                                        <div className="resultDivResult">
                                            <span className="totalResultSpanQuiz">{total}/{totalPossible}</span>
                                        </div>
                                        <div className="quizCompletedDateQuiz">{quizCompletedAt}</div>
                                    </div>
                                    }
                                    {this.state.moreThenTwo &&
                                        <button onClick={this.afterTwoMonths}>Start</button>
                                    }
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageTwoProgressQuiz">
                                    <section>
                                        <div>Step Two</div>
                                        <span>Logic Test</span>
                                    </section>
                                    <p>Not qualified yet</p>
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageTreeProgressQuiz">
                                    <section>
                                        <div>Step Tree</div>
                                        <span>Interview</span>
                                    </section>
                                    <p>Not qualified yet</p>
                                </div>
                                <img src={arrow} alt=""/>
                                <div className="stageFourProgressQuiz">
                                    <section>
                                        <div>Step Four</div>
                                        <span>Training</span>
                                    </section>
                                    <p>Not qualified yet</p>
                                </div>
                            </div>
                        </div>
                    )
                }
            }

        const mapStateToProps = (state) => ({

        });

        const mapDispatchToProps = (dispatch) => ({
            clearLocalStorage: () => dispatch(clearLocalStorage()),
        });

        export default connect(mapStateToProps, mapDispatchToProps)(ProgressQuiz);
