import React from 'react';
import modal from "modals";
class SecondStepRiddle extends React.Component {
    componentWillMount = () => {
         let startTime = () => {
            let time = new Date().getTime();
             localStorage.setItem("riddleStarted" , JSON.stringify(time));
         };
         startTime();
    };
     changeColColor = (e) => {
         let index = e.target.dataset.index;
       let color = document.querySelector(`.col1${index}`).value;
         switch (color) {
             case 'red':
                 document.documentElement.style.setProperty(`--col${index}`, "#FC5652");
                 break;
             case 'yellow':
                 document.documentElement.style.setProperty(`--col${index}`, "#F6C77B");
                 break;
             case 'blue':
                 document.documentElement.style.setProperty(`--col${index}`, "#5888A8");
                 break;
             case 'green':
                 document.documentElement.style.setProperty(`--col${index}`, "#06A08F");
                 break;
             case 'white':
                 document.documentElement.style.setProperty(`--col${index}`, "#505050");
                 break;
             case 'select':
                 document.documentElement.style.setProperty(`--col${index}`, "rgba(77, 127, 164, 0.4)");
                 break;
             default:
         }
    };

     crossLine = (e) => {
         let index = e.target.dataset.index;
         let line = document.querySelector(`.clue${index}`);
         console.log(line);
         if (line.classList.contains("crossed") === true) {
             line.classList.remove('crossed');
         } else {
             line.classList.add('crossed');
         }
     };
timeForRiddle = () => {
    let riddleStarted = JSON.parse(localStorage.getItem("riddleStarted"));
    let timeNow = new Date().getTime();
    let milliseconds = (timeNow - riddleStarted);
    let sec = Math.floor(milliseconds / 1000);
    let min = Math.floor(sec / 60);
    let hour = Math.floor(min / 60);
    sec = sec % 60;
    min = min % 60;
    hour = hour % 24;
    localStorage.setItem("riddleMin" , JSON.stringify(min));
    localStorage.setItem("riddleSec" , JSON.stringify(sec));
    localStorage.setItem("riddleHour" , JSON.stringify(hour));
};


    submitRiddle = () => {
    this.timeForRiddle();
        let checkNumber = 0;
        let col4Col = document.querySelector(".col14").value;
        let col4Nat = document.querySelector(".col24").value;
        let col4Drink = document.querySelector(".col34").value;
        let col4Cig = document.querySelector(".col44").value;
        let col4Pet = document.querySelector(".col54").value;
        let checkCeorrect = () => {
            // eslint-disable-next-line
            col4Col === "green" ? checkNumber ++ : checkNumber;
            // eslint-disable-next-line
            col4Nat === "German" ? checkNumber ++ : checkNumber;
            // eslint-disable-next-line
            col4Drink === "coffee" ? checkNumber ++ : checkNumber;
            // eslint-disable-next-line
            col4Cig === "Prince" ? checkNumber ++ : checkNumber;
            // eslint-disable-next-line
            col4Pet === "fish" ? checkNumber ++ : checkNumber;

            if (checkNumber === 5) {
                        let riddleResult = 1;
                        this.props.question2True();
                        localStorage.setItem("riddleResult" , JSON.stringify(riddleResult));
            } else {
                this.props.question2True();
                let riddleResult = 0 ;
                localStorage.setItem("riddleResult" , JSON.stringify(riddleResult));
            }
        }
        checkCeorrect();
    };

        confirmStartOver = () => {
            modal.confirm({
                message: 'Are you sure, it will reset all your progress',
                callback: (val) => {
                    if(val === 'Ok') {
                        this.startOver();
                    }
                },
                okTitle: 'Ok',
                cancelTitle: 'Cancel'
            });
        };
     startOver = () => {
         document.querySelector(".riddle").reset();
         document.documentElement.style.setProperty(`--col1`, "rgba(77, 127, 164, 0.4)");
         document.documentElement.style.setProperty(`--col2`, "rgba(77, 127, 164, 0.4)");
         document.documentElement.style.setProperty(`--col3`, "rgba(77, 127, 164, 0.4)");
         document.documentElement.style.setProperty(`--col4`, "rgba(77, 127, 164, 0.4)");
         document.documentElement.style.setProperty(`--col5`, "rgba(77, 127, 164, 0.4)");
     };

render() {
    return (
        <div className="riddlePage">
            <div className="topClue">
                There are five houses of different colors next to each other.
                In each house lives a man. Each man has an unique nationality, an exclusive favorite drink,
                a distinct favorite brand of cigarettes and keeps specific pets.
                Use all the clues below to fill the grid and answer the question: "Who owns the fish?"
            </div>
            <div className="allClues">
                <div className="leftClues">
                    <div className="clue1"><span onClick={this.crossLine} data-index="1">x</span> The Brit lives in the Red house.</div>
                    <div className="clue2"><span onClick={this.crossLine} data-index="2">x</span> The Swede keeps Dogs as pets.</div>
                    <div className="clue3"><span onClick={this.crossLine} data-index="3">x</span> The Dane drinks Tea.</div>
                    <div className="clue4"><span onClick={this.crossLine} data-index="4">x</span> The Green house is next to the Grey house, on the left.</div>
                    <div className="clue5"><span onClick={this.crossLine} data-index="5">x</span> The owner of the Green house drinks Coffee.</div>
                    <div className="clue6"><span onClick={this.crossLine} data-index="6">x</span> The person who smokes Pall Mall rears Birds.</div>
                    <div className="clue7"><span onClick={this.crossLine} data-index="7">x</span> The owner of the Yellow house smokes Dunhill.</div>
                    <div className="clue8"><span onClick={this.crossLine} data-index="8">x</span> The man living in the centre house drinks Milk.</div>
                </div>
                <div className="rightClues">
                    <div className="clue9"><span onClick={this.crossLine} data-index="9">x</span> The Norwegian lives in the first house.</div>
                    <div className="clue10"><span onClick={this.crossLine} data-index="10">x</span> The man who smokes Blends lives next to the one who keeps Cats.</div>
                    <div className="clue11"><span onClick={this.crossLine} data-index="11">x</span> The man who keeps Horses lives next to the man who smokes Dunhill.</div>
                    <div className="clue12"><span onClick={this.crossLine} data-index="12">x</span> The man who smokes Blue Master drinks Beer.</div>
                    <div className="clue13"><span onClick={this.crossLine} data-index="13">x</span> The German smokes Prince.</div>
                    <div className="clue14"><span onClick={this.crossLine} data-index="14">x</span> The Norwegian lives next to the Blue house.</div>
                    <div className="clue15"><span onClick={this.crossLine} data-index="15">x</span> The man who smokes Blends has a neighbour who drinks Water.</div>
                </div>
            </div>
            <form className="riddle">
                <div className="houses">
                    <span className="blankTransparent"> </span>
                    <span>House 1</span>
                    <span>House 2</span>
                    <span>House 3</span>
                    <span>House 4</span>
                    <span>House 5</span>
                </div>
                <div className="row1">
                    <span className="1">color</span>
                    <select className="col11" onChange={this.changeColColor} data-index="1">
                        <option value="select">select</option>
                        <option value="blue">blue</option>
                        <option value="green">green</option>
                        <option value="white">grey</option>
                        <option value="red">red</option>
                        <option value="yellow">yellow</option>
                    </select>
                    <select className="col12" onChange={this.changeColColor} data-index="2">
                        <option value="select">select</option>
                        <option value="blue">blue</option>
                        <option value="green">green</option>
                        <option value="white">grey</option>
                        <option value="red">red</option>
                        <option value="yellow">yellow</option>
                    </select>
                    <select className="col13" onChange={this.changeColColor} data-index="3">
                        <option value="select">select</option>
                        <option value="blue">blue</option>
                        <option value="green">green</option>
                        <option value="white">grey</option>
                        <option value="red">red</option>
                        <option value="yellow">yellow</option>
                    </select>
                    <select className="col14" onChange={this.changeColColor} data-index="4">
                        <option value="select">select</option>
                        <option value="blue">blue</option>
                        <option value="green">green</option>
                        <option value="white">grey</option>
                        <option value="red">red</option>
                        <option value="yellow">yellow</option>
                    </select>
                    <select className="col15" onChange={this.changeColColor} data-index="5">
                        <option value="select">select</option>
                        <option value="blue">blue</option>
                        <option value="green">green</option>
                        <option value="white">grey</option>
                        <option value="red">red</option>
                        <option value="yellow">yellow</option>
                    </select>
                </div>
                <div className="row2">
                    <span className="1">nationality</span>
                    <select className="col21">
                        <option value="select">select</option>
                        <option value="Brit">Brit</option>
                        <option value="German">German</option>
                        <option value="Norwegian">Norwegian</option>
                        <option value="Swede">Swede</option>
                        <option value="Dane">Dane</option>
                    </select>
                    <select className="col22">
                        <option value="select">select</option>
                        <option value="Brit">Brit</option>
                        <option value="German">German</option>
                        <option value="Norwegian">Norwegian</option>
                        <option value="Swede">Swede</option>
                        <option value="Dane">Dane</option>
                    </select>
                    <select className="col23">
                        <option value="select">select</option>
                        <option value="Brit">Brit</option>
                        <option value="German">German</option>
                        <option value="Norwegian">Norwegian</option>
                        <option value="Swede">Swede</option>
                        <option value="Dane">Dane</option>
                    </select>
                    <select className="col24">
                        <option value="select">select</option>
                        <option value="Brit">Brit</option>
                        <option value="German">German</option>
                        <option value="Norwegian">Norwegian</option>
                        <option value="Swede">Swede</option>
                        <option value="Dane">Dane</option>
                    </select>
                    <select className="col25">
                        <option value="select">select</option>
                        <option value="Brit">Brit</option>
                        <option value="German">German</option>
                        <option value="Norwegian">Norwegian</option>
                        <option value="Swede">Swede</option>
                        <option value="Dane">Dane</option>
                    </select>
                </div>
                <div className="row3">
                    <span className="1">drink</span>
                    <select className="col31">
                        <option value="select">select</option>
                        <option value="beer">beer</option>
                        <option value="coffee">coffee</option>
                        <option value="tea">tea</option>
                        <option value="milk">milk</option>
                        <option value="water">water</option>
                    </select>
                    <select className="col32">
                        <option value="select">select</option>
                        <option value="beer">beer</option>
                        <option value="coffee">coffee</option>
                        <option value="tea">tea</option>
                        <option value="milk">milk</option>
                        <option value="water">water</option>
                    </select>
                    <select className="col33">
                        <option value="select">select</option>
                        <option value="beer">beer</option>
                        <option value="coffee">coffee</option>
                        <option value="tea">tea</option>
                        <option value="milk">milk</option>
                        <option value="water">water</option>
                    </select>
                    <select className="col34">
                        <option value="select">select</option>
                        <option value="beer">beer</option>
                        <option value="coffee">coffee</option>
                        <option value="tea">tea</option>
                        <option value="milk">milk</option>
                        <option value="water">water</option>
                    </select>
                    <select className="col35">
                        <option value="select">select</option>
                        <option value="beer">beer</option>
                        <option value="coffee">coffee</option>
                        <option value="tea">tea</option>
                        <option value="milk">milk</option>
                        <option value="water">water</option>
                    </select>
                </div>
                <div className="row4">
                    <span className="1">cigarettes</span>
                    <select className="col41" >
                        <option value="select">select</option>
                        <option value="Blends">Blends</option>
                        <option value="Blue Master">Blue Master</option>
                        <option value="Dunhill">Dunhill</option>
                        <option value="Pall Mall">Pall Mall</option>
                        <option value="Prince">Prince</option>
                    </select>
                    <select className="col42">
                        <option value="select">select</option>
                        <option value="Blends">Blends</option>
                        <option value="Blue Master">Blue Master</option>
                        <option value="Dunhill">Dunhill</option>
                        <option value="Pall Mall">Pall Mall</option>
                        <option value="Prince">Prince</option>
                    </select>
                    <select className="col43">
                        <option value="select">select</option>
                        <option value="Blends">Blends</option>
                        <option value="Blue Master">Blue Master</option>
                        <option value="Dunhill">Dunhill</option>
                        <option value="Pall Mall">Pall Mall</option>
                        <option value="Prince">Prince</option>
                    </select>
                    <select className="col44">
                        <option value="select">select</option>
                        <option value="Blends">Blends</option>
                        <option value="Blue Master">Blue Master</option>
                        <option value="Dunhill">Dunhill</option>
                        <option value="Pall Mall">Pall Mall</option>
                        <option value="Prince">Prince</option>
                    </select>
                    <select className="col45">
                        <option value="select">select</option>
                        <option value="Blends">Blends</option>
                        <option value="Blue Master">Blue Master</option>
                        <option value="Dunhill">Dunhill</option>
                        <option value="Pall Mall">Pall Mall</option>
                        <option value="Prince">Prince</option>
                    </select>
                </div>
                <div className="row5">
                    <span className="1">pets</span>
                    <select className="col51">
                        <option value="select">select</option>
                        <option value="dogs">dogs</option>
                        <option value="cats">cats</option>
                        <option value="birds">birds</option>
                        <option value="horses">horses</option>
                        <option value="fish">fish</option>
                    </select>
                    <select className="col52">
                        <option value="select">select</option>
                        <option value="dogs">dogs</option>
                        <option value="cats">cats</option>
                        <option value="birds">birds</option>
                        <option value="horses">horses</option>
                        <option value="fish">fish</option>
                    </select>
                    <select className="col53">
                        <option value="select">select</option>
                        <option value="dogs">dogs</option>
                        <option value="cats">cats</option>
                        <option value="birds">birds</option>
                        <option value="horses">horses</option>
                        <option value="fish">fish</option>
                    </select>
                    <select className="col54">
                        <option value="select">select</option>
                        <option value="dogs">dogs</option>
                        <option value="cats">cats</option>
                        <option value="birds">birds</option>
                        <option value="horses">horses</option>
                        <option value="fish">fish</option>
                    </select>
                    <select className="col55">
                        <option value="select">select</option>
                        <option value="dogs">dogs</option>
                        <option value="cats">cats</option>
                        <option value="birds">birds</option>
                        <option value="horses">horses</option>
                        <option value="fish">fish</option>
                    </select>
                </div>
                <button type="button" className="resetRiddleButton" onClick={this.confirmStartOver}>Start over</button>
                <button type="button" className="submitRiddleButton" onClick={this.submitRiddle}>Submit</button>
            </form>
        </div>
    )
}
};


export default SecondStepRiddle;