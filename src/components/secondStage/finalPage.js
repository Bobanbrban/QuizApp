import React from 'react';
import db from "../../firebase/firebase";
import dbClone from "../../firebase/firebaseClone";

class FinalPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: false,
            addedAlready: false
        }
    }

submitComment = () => {
    let curp = JSON.parse(localStorage.getItem("curp"));
    let comment = document.querySelector(".commentAtTheEnd").value;

    let docRef = db.doc(`users/${curp}`);
    let docRefClone = dbClone.doc(`users/${curp}`);
   docRef.get()
       .then(doc => {
           if(doc.exists) {
               docRef.update({comment});
               docRefClone.update({comment});
           }
       });
    this.setState({
        comment: false,
        addedAlready: true
    });
};

    noComment = () => {
        this.setState({
            comment: false
        });
    };

    openCommentDiv = () => {
        this.setState({
            comment: true
        });
    };

    render() {
        return (
            <div className="finalPage">
                <div className="finalDiv">
                    <div className="finalText">
                        Congratulations, you made it !!!
                        After reviewing your answer, we will get back to you. No matter what is our response,
                        you made it to the very top of our selection, that is great result.
                        In mean time, we advice you to check more about Front-end developing and Back-end developing,
                        and try to see what suits you more. In case of our positive response,
                        it will help us to place you in proper training group as soon as possible
                        and start working on your progress in wright direction straight away
                    </div>
                    {(!this.state.comment && !this.state.addedAlready) &&
                    <div className="openCommentDiv" onClick={this.openCommentDiv}>I would like to add comment</div>
                    }
                    { this.state.comment &&
                    <form className="comment">
                        <textarea type="text" className="commentAtTheEnd"/>
                        <button type="button" className="submitFinalComment" onClick={this.submitComment}>Submit comment
                        </button>
                        <button type="button" className="noFinalComment" onClick={this.noComment}>No comment
                        </button>
                    </form>
                    }
                </div>
            </div>
        )
    }
}


export default FinalPage;