import React from 'react';
import connect from "react-redux/es/connect/connect";
import db from "../../firebase/firebase";
import ProgressQuiz from "./progressBarPages/progressQuizOnly";
import ProgressQualifiedForSecond from "./progressBarPages/progressBarSecond";
import ProgressQualifiedForInterview from "./progressBarPages/progressBarInterview";
import ProgressQualifiedForTraining from "./progressBarPages/progressQualifiedTraining";
import LoadingGif from "./../loadingGif";
import {
    quizFailed,
    secondStepQualified,
    secondStepCompleted,
    qualifiedForInterview,
    qualifiedForTraining,
    riddleAction
} from '../../actions/secondStageActions';
import { secondStagePage, loginPage } from '../../actions/pagesActions';
import modal from "modals";

class ProgressPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            quiz: false,
            forSecond: false,
            secondCompleted: false,
            qualifiedInterview: false,
            qualifiedTraining: false,
            quizCreatedAt: '',
            secondCompletedAt: '',
            curp: this.props.curp,
        }
    }

    componentWillMount() {
        let curp = this.props.curp;
        if(curp !== ""){
            let docRef = db.doc(`users/${curp}`);
            let progress = {progressBarLoaded: true};
            docRef.update(progress);
            docRef.get()
                .then(doc => {
                    let stage = 0;
                    let completedSecond = doc.data().valueOf().secondStepCompleted;
                    let qualifiedForInterview = doc.data().valueOf().invitedForInterview;
                    let qualifiedForTraining = doc.data().valueOf().qualifiedForTraining;
                    let secondCompletedAt = doc.data().valueOf().secondCompletedAt;
                    let quizCompletedDate = doc.data().valueOf().createdAt;
                    let interviewDate = doc.data().valueOf().interviewDate;
                    let secondResult = doc.data().valueOf().secondStepResult;
                    let secondTime = doc.data().valueOf().secondStepTime;
                    let total = doc.data().valueOf().total;
                    this.setState({quizCreatedAt: quizCompletedDate});
                    // eslint-disable-next-line
                    total > 59 ? stage = 1 : stage;
                    // eslint-disable-next-line
                    completedSecond === true ? stage = 2 : stage;
                    // eslint-disable-next-line
                    qualifiedForInterview === true ? stage = 3 : stage;
                    // eslint-disable-next-line
                    qualifiedForTraining === true ? stage = 4 : stage;
                    switch(stage) {
                        case 4:
                            this.setState({qualifiedTraining: true, loading: false, secondCompletedAt, interviewDate, secondResult, secondTime,total});
                            console.log(stage);
                            break;
                        case 3:
                            this.setState({qualifiedInterview: true, loading: false , secondCompletedAt, interviewDate, secondResult, secondTime,total});
                            console.log(stage);
                            break;
                        case 2:
                            this.setState({forSecond: true, secondCompleted: true, loading: false, secondCompletedAt, secondResult, secondTime,total});
                            console.log(stage);
                            break;
                        case 1:
                            this.setState({forSecond: true, loading: false,total});
                            break;
                        case 0:
                            this.setState({quiz: true, loading: false,total});
                            break;
                        default:
                    }
                })
        } else {
            this.props.loginPage();
        }
    }


    riddleTrue = () => {
        if(window.innerWidth > 1000){
            this.setState({
                quiz: false,
                forSecond: false,
                secondCompleted: false,
                qualifiedInterview: false,
                qualifiedTraining: false,
            });
            this.props.secondStagePage();
            this.props.riddleAction();
        } else {
            modal.alert({
                message: 'Minimum required screen width for this test is 1000px',
                callback: function () {
                    console.log('Minimum required screen width for this test is 1000px')
                },
                okTitle: 'OK'
            })
        }
    };


    render() {
        return (
            <div className="ProgressPage">
                { this.state.loading &&
                <LoadingGif />
                }
                { this.state.quiz &&
                <ProgressQuiz  state={this.state} getQuestions={this.props.getQuestions} />
                }
                { this.state.forSecond &&
                <ProgressQualifiedForSecond  state={this.state} riddleTrue={this.riddleTrue}/>
                }
                { this.state.qualifiedInterview &&
                <ProgressQualifiedForInterview  state={this.state}/>
                }
                { this.state.qualifiedTraining &&
                <ProgressQualifiedForTraining  state={this.state}/>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    curp: state.order.curp,
    quiz: state.progress.quiz,
    forSecond: state.progress.forSecond,
    completedSecond: state.progress.completedSecond,
    qualifiedInterview: state.progress.qualifiedInterview,
    qualifiedTraining: state.progress.qualifiedTraining,
});

const mapDispatchToProps = (dispatch) => ({
    quizFailed: () => dispatch(quizFailed),
    secondStepQualified: () => dispatch(secondStepQualified),
    secondStepCompleted: () => dispatch(secondStepCompleted),
    qualifiedForInterview: () => dispatch(qualifiedForInterview),
    qualifiedForTraining: () => dispatch(qualifiedForTraining),
    riddleAction: () => dispatch(riddleAction()),
    secondStagePage: () => dispatch(secondStagePage()),
    loginPage: () => dispatch(loginPage())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProgressPage);