import React from 'react';
import modal from "modals";


class SecondStepQuestion2 extends React.Component {
    componentWillMount () {
        let startTime = () => {
            let time = new Date().getTime();
            localStorage.setItem("question2Started" , JSON.stringify(time));
        };
        startTime();
    }
timeForSecond = () => {
    let question2Started = JSON.parse(localStorage.getItem("question2Started"));
    let riddleStarted = JSON.parse(localStorage.getItem("riddleStarted"));
    let timeNow = new Date().getTime();
    let milliseconds = (timeNow - question2Started);
    let sec = Math.floor(milliseconds / 1000);
    let min = Math.floor(sec / 60);
    let hour = Math.floor(min / 60);
    sec = sec % 60;
    min = min % 60;
    hour = hour % 24;
    localStorage.setItem("question2Min" , JSON.stringify(min));
    localStorage.setItem("question2Sec" , JSON.stringify(sec));
    localStorage.setItem("question2Hour" , JSON.stringify(hour));
    let millisecondsTotal = (timeNow - riddleStarted);
    let secTotal = Math.floor(millisecondsTotal / 1000);
    let minTotal = Math.floor(secTotal / 60);
    let hourTotal = Math.floor(minTotal / 60);
    secTotal = secTotal % 60;
    minTotal = minTotal % 60;
    hourTotal = hourTotal % 24;
    localStorage.setItem("totalMin" , JSON.stringify(minTotal));
    localStorage.setItem("totalSec" , JSON.stringify(secTotal));
    localStorage.setItem("totalHour" , JSON.stringify(hourTotal));
};
    submitBuckets = () => {
        this.timeForSecond();
        let fullAnswer = document.querySelector('.answerBuckets').value;
        let finalAnswer = document.querySelector('.answerLittersInput').value;
        localStorage.setItem("question2FullAnswer" , JSON.stringify(fullAnswer));
        localStorage.setItem("question2FinalAnswer" , JSON.stringify(finalAnswer));
        let isCorrect;
         finalAnswer === "3"? isCorrect = "Correct" : isCorrect = "Not correct";
         let completed;
        fullAnswer === "" || finalAnswer === "" ? completed = false : completed = true;
        if (completed) {
                    let question2Result = isCorrect === "Correct" ? 1 : 0;
                    localStorage.setItem("question2Result" , JSON.stringify(question2Result));
                   this.props.question3True();
        } else {
            modal.alert({
                message: `Please fill all answers`,
                callback: function () {
                    console.log('bucket');
                },
                okTitle: 'OK'
            });
        }
        }


    render() {
        return (
            <div className="question2Page">
            <div className="questionsBuckets">If you had an infinite supply of water and a 5-liters and 3-liters bucket,
               how would you measure exactly 4 liters, with minimum water wasted ? The buckets do not have any intermediate scales.
               Write down all the steps and find out how many litters of water has been wasted , not counting the water that stays in the buckets.</div>
                <textarea type="text" className="answerBuckets" />
                <div className="answerLittersWater">
                    <span className="littersQuestion">How many liters of water has been wasted ?</span>
                    <input type="text" className="answerLittersInput" required /><span className="litters">liters</span>
                    <button className="submitQuestionsBuckets" onClick={this.submitBuckets}>Submit all</button>
                </div>
            </div>
        )
    }
}


export default SecondStepQuestion2;