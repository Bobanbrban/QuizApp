import React from 'react';
import SecondStepRiddle from "./secondStepRiddle";
import SecondStepQuestion2 from "./secondStepQuestion2";
import SecondStepQuestion3 from "./secondStepQuestion3";
import FinalPage from "./finalPage";
import connect from "react-redux/es/connect/connect";
import {
    riddleAction,
    question2Action,
    question3Action,
    finalPageAction,
    backToResults
} from '../../actions/secondStageActions'
import db from "../../firebase/firebase";
import modal from "modals";


class SecondStep extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            qualified: {
                forSecond: false,
                completedSecond: false,
                qualifiedForInterview: false,
                qualifiedForTraining: false
            },
           userInfoObject: {
               date: "",
           }
        }
    }

    componentWillMount() {
        let curp = this.props.curp;
        let docRef = db.doc(`users/${curp}`);
        docRef.get()
            .then(doc => {
                let date = doc.data().valueOf().createdAt;
                let secondQualified = doc.data().valueOf().secondStepQualified;
                let secondQompleted = doc.data().valueOf().secondStepCompleted;
                let qualifiedInterview = doc.data().valueOf().qualifiedInterview;
                this.setState({
                    date,
                })
                if (secondQualified === true) {
                    this.setState({
                        qualified: {
                            forSecond: true,
                            completedSecond: false,
                            qualifiedForInterview: false,
                            qualifiedForTraining: false
                        },
                    })
                }
                if(secondQompleted) {
                    this.setState({
                        qualified: {
                            forSecond: true,
                            completedSecond: true,
                            qualifiedForInterview: false,
                            qualifiedForTraining: false
                        },
                    })
                }
                if(qualifiedInterview) {
                    this.setState({
                        qualified: {
                            forSecond: true,
                            completedSecond: true,
                            qualifiedForInterview: true,
                            qualifiedForTraining: false
                        },
                    })
                }
            })
    }


    riddleTrue = () => {
        if(window.innerWidth > 1000){
            this.props.riddleAction();
        } else {
            modal.alert({
                message: 'Required minimum screen width is 1000px',
                callback: function () {
                    console.log('Required minimum screen width is 1000px')
                },
                okTitle: 'OK'
            })
        }

    };
    question2True = () => {
        this.props.question2Action();
    };
    question3True = () => {
        this.props.question3Action();
    };
    finalPageFunction = () => {
        this.props.finalPageAction();
    };




    render() {
        return (
            <div className="secondStagePage">
                { this.props.riddleTrue &&
                <SecondStepRiddle question2True={this.question2True}/>
                }
                { this.props.question2True &&
                <SecondStepQuestion2 question3True={this.question3True} />
                }
                { this.props.question3True &&
                <SecondStepQuestion3  finalPageFunction={this.finalPageFunction}/>
                }
                { this.props.finalPage && <FinalPage />}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    riddleTrue: state.secondStage.riddle,
    question2True: state.secondStage.question2,
    question3True: state.secondStage.question3,
    finalPage: state.secondStage.finalPage,
    infoTrue: state.secondStage.candidateInfo,
    curp: state.order.curp,
    quizCompleted: state.secondStage.quizCompleted,
    secondStepQualified: state.secondStage.secondStepQualified,
    thirdStepTrue: state.secondStage.thirdStepTrue,
});

const mapDispatchToProps = (dispatch) => ({
    riddleAction: () => dispatch(riddleAction()),
    question2Action: () => dispatch(question2Action()),
    question3Action: () => dispatch(question3Action()),
    finalPageAction: () => dispatch(finalPageAction()),
    backToResults: () => dispatch(backToResults())
});

export default connect(mapStateToProps, mapDispatchToProps)(SecondStep);