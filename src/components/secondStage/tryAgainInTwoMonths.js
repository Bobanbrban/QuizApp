import React from 'react';
import { connect } from 'react-redux';



class ThankYouPage extends React.Component {

    render() {
        return (
            <div className="thankYouPage">
                <div className="tryAgainInTwoMonths">
                    <div className="twoMonthsText">
                        Thank you for your effort. At this moment you haven't qualified for next stage,
                         but you are more than welcome to try again in 2 months time.
                    </div>
                    <button className="closeTwoMonthsButton" onClick={this.props.clearFirstTime}>Got it</button>
                </div>
            </div>
        )
    }
};

const mapStatetoProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({

});


export default connect( mapStatetoProps, mapDispatchToProps )(ThankYouPage);
