import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import {connect} from "react-redux";
import createHistory from 'history/createBrowserHistory';
import QuestionBody from './firstStage/questionBody-MAIN';
import Admin from "./adminQuestions/admin";
import AgaveTeam from "./adminQuestions/AgaveTeam";
import ResultsAdmin from "./adminResults/candidatesAdmin";



export const history = createHistory();

const Quiz = () => (
    <Router history={history}>
        <div>
            <Switch>
                <Route path="/" component={ QuestionBody } exact={true}/>
                <Route path="/admin" component={Admin}/>
                <Route path="/agavelab" component={AgaveTeam}/>
                <Route path="/candidates" component={ResultsAdmin}/>
            </Switch>
        </div>
    </Router>
);


const mapStatetoProps = (state) => ({
   completed: state.order.completed
});


export default connect(mapStatetoProps)(Quiz);
