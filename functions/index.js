function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({ origin: true });
let _ = require('lodash');
const postmark = require("postmark");

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');

const client = new postmark.Client("39465aef-4f8b-4186-a0fd-b94da3a3e667");

admin.initializeApp({
    apiKey: "AIzaSyAy86vd-Lp-WqHhIRuScDLs0SsdyFsFQ-k",
    authDomain: "agavequiz-main.firebaseapp.com",
    credential: admin.credential.applicationDefault(),
    databaseURL: "https://agavequiz-main.firebaseio.com"
});

exports.getRundomQuestions = functions.https.onRequest((request, response) => {
    cors(request, response, () => {});
    let allDataSpecial = [];
    let allDatalogical = [];
    let allDatanumerical = [];
    let allDatacomplex = [];
    let allDatapattern = [];
    let special = [];
    let logical = [];
    let numerical = [];
    let complex = [];
    let pattern = [];
    let specialNum;
    let logicalNum;
    let numericalNum;
    let complexNum;
    let patternNum;
    admin.firestore().doc("databaseSet/settings").get().then((() => {
        var _ref = _asyncToGenerator(function* (data) {
            specialNum = yield data.data().special;
            logicalNum = yield data.data().logical;
            numericalNum = yield data.data().numerical;
            complexNum = yield data.data().complex;
            patternNum = yield data.data().pattern;
        });

        return function (_x) {
            return _ref.apply(this, arguments);
        };
    })()).then(_asyncToGenerator(function* () {
        let snapshotSpecial = yield admin.firestore().collection("questionGroup/special/questions").get();
        snapshotSpecial.forEach((() => {
            var _ref3 = _asyncToGenerator(function* (data) {
                let questionObject = data.data().question;
                special.push(questionObject);
                let specialFive = yield special;
                let shuffledSpec = _.shuffle(specialFive);
                let i;
                for (i = 0; i < specialNum; i++) {
                    let randomSpec = shuffledSpec[i];
                    if (allDataSpecial.length < specialNum && randomSpec !== null) {
                        allDataSpecial.push(randomSpec);
                    }
                }
            });

            return function (_x2) {
                return _ref3.apply(this, arguments);
            };
        })());

        let snapshotLogical = yield admin.firestore().collection("questionGroup/logical/questions").get();
        snapshotLogical.forEach((() => {
            var _ref4 = _asyncToGenerator(function* (data) {
                let questionObject = data.data().question;
                logical.push(questionObject);
                let logicalFive = yield logical;
                let shuffledLog = _.shuffle(logicalFive);
                let i;
                for (i = 0; i < logicalNum; i++) {
                    let randomLog = shuffledLog[i];
                    if (allDatalogical.length < logicalNum && randomLog !== null) {
                        allDatalogical.push(randomLog);
                    }
                }
            });

            return function (_x3) {
                return _ref4.apply(this, arguments);
            };
        })());
        let snapshotNumerical = yield admin.firestore().collection("questionGroup/numerical/questions").get();
        snapshotNumerical.forEach((() => {
            var _ref5 = _asyncToGenerator(function* (data) {
                let questionObject = data.data().question;
                numerical.push(questionObject);
                let numericalFive = yield numerical;
                let shuffledNum = _.shuffle(numericalFive);
                let i;
                for (i = 0; i < numericalNum; i++) {
                    let randomNum = shuffledNum[i];
                    if (allDatanumerical.length < numericalNum && randomNum !== null) {
                        allDatanumerical.push(randomNum);
                    }
                }
            });

            return function (_x4) {
                return _ref5.apply(this, arguments);
            };
        })());
        let snapshotComplex = yield admin.firestore().collection("questionGroup/complex/questions").get();
        snapshotComplex.forEach((() => {
            var _ref6 = _asyncToGenerator(function* (data) {
                let questionObject = data.data().question;
                complex.push(questionObject);
                let complexFive = yield complex;
                let shuffledCom = _.shuffle(complexFive);
                let i;
                for (i = 0; i < complexNum; i++) {
                    let randomCom = shuffledCom[i];
                    if (allDatacomplex.length < complexNum && randomCom !== null) {
                        allDatacomplex.push(randomCom);
                    }
                }
            });

            return function (_x5) {
                return _ref6.apply(this, arguments);
            };
        })());
        let snapshotPattern = yield admin.firestore().collection("questionGroup/pattern/questions").get();
        snapshotPattern.forEach((() => {
            var _ref7 = _asyncToGenerator(function* (data) {
                let questionObject = data.data().question;
                pattern.push(questionObject);
                let patternFive = yield pattern;
                let shuffledPat = _.shuffle(patternFive);
                let i;
                for (i = 0; i < patternNum; i++) {
                    let randomPat = shuffledPat[i];
                    if (allDatapattern.length < patternNum && randomPat !== null) {
                        allDatapattern.push(randomPat);
                    }
                }
            });

            return function (_x6) {
                return _ref7.apply(this, arguments);
            };
        })());
        let awaitedS = yield allDataSpecial;
        let awaitedN = yield allDatanumerical;
        let awaitedL = yield allDatalogical;
        let awaitedC = yield allDatacomplex;
        let awaitedP = yield allDatapattern;

        let newArray = [...awaitedS, ...awaitedN, ...awaitedL, ...awaitedC, ...awaitedP];

        return response.send(newArray);
    })).catch(error => {
        console.log(error);
        response.status(500).send(error);
    });
});

exports.adminAllQuestions = functions.https.onRequest((() => {
    var _ref8 = _asyncToGenerator(function* (request, response) {
        cors(request, response, function () {});
        let special = [];
        let logical = [];
        let numerical = [];
        let complex = [];
        let pattern = [];
        let snapshotSpecial = yield admin.firestore().collection("questionGroup/special/questions").get();
        snapshotSpecial.forEach(function (data) {
            let questionObject = data.data().question;
            special.push(questionObject);
        });
        let snapshotLogical = yield admin.firestore().collection("questionGroup/logical/questions").get();
        snapshotLogical.forEach(function (data) {
            let questionObject = data.data().question;
            logical.push(questionObject);
        });
        let snapshotNumerical = yield admin.firestore().collection("questionGroup/numerical/questions").get();
        snapshotNumerical.forEach(function (data) {
            let questionObject = data.data().question;
            numerical.push(questionObject);
        });
        let snapshotComplex = yield admin.firestore().collection("questionGroup/complex/questions").get();
        snapshotComplex.forEach(function (data) {
            let questionObject = data.data().question;
            complex.push(questionObject);
        });
        let snapshotPattern = yield admin.firestore().collection("questionGroup/pattern/questions").get();
        snapshotPattern.forEach(function (data) {
            let questionObject = data.data().question;
            pattern.push(questionObject);
        });
        let awaitedS = yield special;
        let awaitedN = yield numerical;
        let awaitedL = yield logical;
        let awaitedC = yield complex;
        let awaitedP = yield pattern;

        let newArray = [...awaitedS, ...awaitedN, ...awaitedL, ...awaitedC, ...awaitedP];

        return response.send(newArray);
    });

    return function (_x7, _x8) {
        return _ref8.apply(this, arguments);
    };
})());

exports.sendEmail = functions.firestore.document('/interview/{uid}').onCreate(snap => {
    let user = snap.data();

    let htmlFull = `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
 <head>
          <!-- NAME: SELL PRODUCTS -->
          <!--[if gte mso 15]>
          <xml>
               <o:OfficeDocumentSettings>
               <o:AllowPNG/>
               <o:PixelsPerInch>96</o:PixelsPerInch>
               </o:OfficeDocumentSettings>
          </xml>
          <![endif]-->
          <meta charSet="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>Dev Factory</title>
        
   </head>
    <body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
          <!--*|IF:MC_PREVIEW_TEXT|*-->
          <!--[if !gte mso 9]><!----><span className="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Interview</span><!--<![endif]-->
          <!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellPadding="0" cellSpacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;">
                <tr>
                    <td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellPadding="0" cellSpacing="0" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                   <tr>
                                        <td align="center" valign="top" id="templateHeader" data-template-container style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0px;">
                                             <!--[if (gte mso 9)|(IE)]>
                                             <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                             <tr>
                                             <td align="center" valign="top" width="600" style="width:600px;">
                                             <![endif]-->
                                             <table align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" className="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;">
                                                  <tr>
                                               <td valign="top" className="headerContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: transparent;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;"><table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody className="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" className="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellPadding="0" cellSpacing="0" className="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tbody><tr>
                            <td className="mcnImageContent" valign="top" style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                
                                    <a href="http://quiz.agavelab.com" title="" className="" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/1379d25728df74bf843708f2f/images/82e57c67-a827-497b-8bb5-16df481149ef.jpg" width="564" style="max-width: 1200px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" className="mcnImage">
                                    </a>
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                                                  </tr>
                                             </table>
                                             <!--[if (gte mso 9)|(IE)]>
                                             </td>
                                             </tr>
                                             </table>
                                             <![endif]-->
                                        </td>
                            </tr>
                                   <tr>
                                        <td align="center" valign="top" background-color="#244d68" id="templateBody" data-template-container style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #244d68;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 94px;padding-bottom: 94px;">
                                             <!--[if (gte mso 9)|(IE)]>
                                             <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                             <tr>
                                             <td align="center" valign="top" width="600" style="width:600px;">
                                             <![endif]-->
                                             <table align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" className="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;">
                                                  <tr>
                                               <td valign="top" className="bodyContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: transparent;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;"><table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody className="mcnTextBlockOuter">
        <tr>
            <td valign="top" className="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                   <!--[if mso]>
                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                    <tr>
                    <![endif]-->
                   
                    <!--[if mso]>
                    <td valign="top" width="600" style="width:600px;">
                    <![endif]-->
                <table align="left" border="0" cellPadding="0" cellSpacing="0" style="padding-right:18px;padding-left:18px;max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" className="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" className="mcnTextContent" style="padding-right:18px;padding-left:18px;margin: 10px 0;font-family: Georgia;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #808080;font-size: 16px;line-height: 150%;text-align: left;">
                        
                            <h4 style="text-align: center;display: block;margin: 15px 0;padding: 0;color: #999999;font-family: Georgia;font-size: 20px;font-style: italic;font-weight: normal;line-height: 125%;letter-spacing: normal;" white=""><span style="color:#FFFFFF"> Congratulations ${user.name}!</span></h4>
                             
   

<p style="text-align: justify;padding-right:18px;padding-left:18px;margin: 10px 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #808080;font-size: 15px;line-height: 150%;"><span style="color:#ffffff;padding-right:18px;padding-left:18px;margin: 10px 0;text-align: justify;font-family:times new roman;margin: 10px 0;padding: 0;font-size: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;"> You have been completed first two tests successfully with great results and we would like to meet you on ${user.interviewDate ? user.interviewDate : ''} for an interview.&#160;</span></p>

                        </td>
                    </tr>
                </tbody></table>
                    <!--[if mso]>
                    </td>
                    <![endif]-->
                
                    <!--[if mso]>
                    </tr>
                    </table>
                    <![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;">
    <tbody className="mcnDividerBlockOuter">
        <tr>
            <td className="mcnDividerBlockInner" style="min-width: 100%;padding: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <table className="mcnDividerContent" border="0" cellPadding="0" cellSpacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tbody><tr>
                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
                                                  </tr>
                                             </table>
                                             <!--[if (gte mso 9)|(IE)]>
                                             </td>
                                             </tr>
                                             </table>
                                             <![endif]-->
                                        </td>
                            </tr>
                            <tr>
                                        <td align="center" valign="top" background-color="#244d68" id="templateFooter" data-template-container style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #244d68;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 0px;">
                                             <!--[if (gte mso 9)|(IE)]>
                                             <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                             <tr>
                                             <td align="center" valign="top" width="600" style="width:600px;">
                                             <![endif]-->
                                             <table align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" className="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;">
                                                  <tr>
                                               <td valign="top" className="footerContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: transparent;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;"><table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody className="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" className="mcnFollowBlockInner">
                <table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody><tr>
        <td align="center" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
            <table border="0" cellPadding="0" cellSpacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" className="mcnFollowContent">
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <table align="center" border="0" cellPadding="0" cellSpacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody><tr>
                                <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellPadding="0" cellSpacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" className="mcnFollowContentItemContainer">
                                                        <table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <table align="left" border="0" cellPadding="0" cellSpacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" className="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="https://www.facebook.com/AgaveLab" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-facebook-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" className=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellPadding="0" cellSpacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" className="mcnFollowContentItemContainer">
                                                        <table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <table align="left" border="0" cellPadding="0" cellSpacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" className="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="https://www.instagram.com/agavelab/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-instagram-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" className=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellPadding="0" cellSpacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" className="mcnFollowContentItemContainer">
                                                        <table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <table align="left" border="0" cellPadding="0" cellSpacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" className="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="quiz.agavelab.com" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-link-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" className=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellPadding="0" cellSpacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" className="mcnFollowContentItemContainer">
                                                        <table border="0" cellPadding="0" cellSpacing="0" width="100%" className="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <table align="left" border="0" cellPadding="0" cellSpacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" className="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <a href="mailto:training@agavelab.com" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-forwardtofriend-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" className=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table></td>
                                                  </tr>
                                             </table>
                                             <!--[if (gte mso 9)|(IE)]>
                                             </td>
                                             </tr>
                                             </table>
                                             <![endif]-->
                                        </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>`;

    const msg = {
        to: user.email,
        from: 'training@agavelab.com',
        subject: 'Dev Factory',
        html: htmlFull
    };

    const msgCopy = {
        to: 'bobanbrban@gmail.com',
        from: 'training@agavelab.com',
        subject: 'Dev Factory',
        html: htmlFull
    };

    let sendTwoMails = (msg, msgCopy) => {
        sgMail.send(msg).then(data => console.log("msg has been sent to the user", data)).catch(err => console.error(err));
        sgMail.send(msgCopy).then(data => console.log("msgCopy has been sent to the user", data)).catch(err => console.error(err));
    };

    return sendTwoMails(msg, msgCopy);
});

exports.sendEmailTotal = functions.firestore.document('/totalMail/{uid}').onCreate(snap => {
    let data = snap.data();
    let user = data.user;
    let name = user.displayName;
    let mail = user.email;
    let createdAt = data.createdAt;
    let total = data.total < 60;
    let failText = `Thank you for taking the test! As a reminder, the date of your last attempt was ${data.createdAt ? data.createdAt : ''}.  You can try again in two months. Thanks`;
    let succesText = `Congratulations, you have achieved great result, which grants you access to the second test. You can complete it within next 7 days. Good luck!!!`;

    let htmlMail = `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
     <head>
          <!-- NAME: SELL PRODUCTS -->
          <!--[if gte mso 15]>
          <xml>
               <o:OfficeDocumentSettings>
               <o:AllowPNG/>
               <o:PixelsPerInch>96</o:PixelsPerInch>
               </o:OfficeDocumentSettings>
          </xml>
          <![endif]-->
          <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>Dev Factory</title>
        </head>
    <body style="height:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
          <!--*|IF:MC_PREVIEW_TEXT|*-->
          <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none !important;font-size:0px;line-height:0px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;visibility:hidden;mso-hide:all;" >Quiz Result</span><!--<![endif]-->
          <!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;height:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;" >
                <tr>
                    <td align="center" valign="top" id="bodyCell" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;height:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;" >
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                   <tr>
                                        <td align="center" valign="top" id="templateHeader" data-template-container style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top-width:0;border-bottom-width:0;padding-top:0px;padding-bottom:0px;" >
                                             <!--[if (gte mso 9)|(IE)]>
                                             <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                             <tr>
                                             <td align="center" valign="top" width="600" style="width:600px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                             <![endif]-->
                                             <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;max-width:600px !important;" >
                                                  <tr>
                                               <td valign="top" class="headerContainer" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:transparent;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top-width:0;border-bottom-width:0;padding-top:0;padding-bottom:0;" ><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top"  class="mcnImageBlockInner" style="padding-top:9px;padding-bottom:9px;padding-right:9px;padding-left:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                
                                    <a href="http://quiz.agavelab.com" title="" class="" target="_blank" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/1379d25728df74bf843708f2f/images/82e57c67-a827-497b-8bb5-16df481149ef.jpg" width="564"  class="mcnImage" style="max-width:1200px;padding-bottom:0;display:inline !important;vertical-align:bottom;-ms-interpolation-mode:bicubic;border-width:0;height:auto;outline-style:none;text-decoration:none;" >
                                    </a>
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                                                  </tr>
                                             </table>
                                             <!--[if (gte mso 9)|(IE)]>
                                             </td>
                                             </tr>
                                             </table>
                                             <![endif]-->
                                        </td>
                            </tr>
                                   <tr>
                                        <td align="center" valign="top" id="templateBody" data-template-container style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#244d68;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top-width:0;border-bottom-width:0;padding-top:94px;padding-bottom:94px;" >
                                             <!--[if (gte mso 9)|(IE)]>
                                             <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                             <tr>
                                             <td align="center" valign="top" width="600" style="width:600px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                             <![endif]-->
                                             <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;max-width:600px !important;" >
                                                  <tr>
                                               <td valign="top" class="bodyContainer" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:transparent;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top-width:0;border-bottom-width:0;padding-top:0;padding-bottom:0;" ><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                   <!--[if mso]>
                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                    <tr>
                    <![endif]-->
                   
                    <!--[if mso]>
                    <td valign="top" width="600" style="width:600px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                    <![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0"  width="100%" class="mcnTextContentContainer" style="max-width:100%;min-width:100%;padding-right:18px;padding-left:18px;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:0px;padding-bottom:9px;padding-right:18px;padding-left:18px;font-family:Helvetica;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;word-break:break-word;color:#808080;font-size:16px;line-height:150%;text-align:left;" >
                        
                            <h4  style="text-align:center;display:block;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;color:#999999;font-family:Georgia;font-size:18px;font-weight:normal;line-height:125%;letter-spacing:normal;" ><span style="text-align:center;display:block;margin-top:0;margin-bottom:30px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;font-size:20px;color:#FFFFFF;font-family:Georgia;font-size:18px;font-weight:normal;line-height:125%;letter-spacing:normal;" > Hi ${user.displayName}, your result is ${data.total}/100</span></h4>
                             
   

<p style="text-align:justify;font-family:times new roman;margin-top:10px;margin-bottom:10px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-size:15px;line-height:150%;color:#FFFFFF">
<span style="text-align:justify;font-family:times new roman;margin-top:10px;margin-bottom:10px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-size:15px;line-height:150%;color:#FFFFFF">${total ? failText : succesText}</span></p>

                        </td>
                    </tr>
                </tbody></table>
                    <!--[if mso]>
                    </td>
                    <![endif]-->
                
                    <!--[if mso]>
                    </tr>
                    </table>
                    <![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:fixed !important;" >
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%;padding-top:18px;padding-bottom:18px;padding-right:18px;padding-left:18px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                    <tbody><tr>
                        <td style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding-top:18px;padding-bottom:18px;padding-right:18px;padding-left:18px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                <hr class="mcnDividerContent" style="border-bottom-color:none;border-left-color:none;border-right-color:none;border-bottom-width:0;border-left-width:0;border-right-width:0;margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
                                                  </tr>
                                             </table>
                                             <!--[if (gte mso 9)|(IE)]>
                                             </td>
                                             </tr>
                                             </table>
                                             <![endif]-->
                                        </td>
                            </tr>
                            <tr>
                                        <td align="center" valign="top" id="templateFooter" data-template-container background-color="#244d68" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#244d68;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top-width:0;border-bottom-width:0;padding-top:0px;padding-bottom:0px;" >
                                             <!--[if (gte mso 9)|(IE)]>
                                             <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                             <tr>
                                             <td align="center" valign="top" width="600" style="width:600px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                             <![endif]-->
                                             <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" background-color="transparent" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;max-width:600px !important;" >
                                                  <tr>
                                               <td valign="top" class="footerContainer" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:transparent;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top-width:0;border-bottom-width:0;padding-top:0;padding-bottom:0;" ><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top"  class="mcnFollowBlockInner" style="padding-top:9px;padding-bottom:9px;padding-right:9px;padding-left:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
    <tbody><tr>
        <td align="center" style="padding-left:9px;padding-right:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
            <table border="0" cellpadding="0" cellspacing="0" width="100%"  class="mcnFollowContent" style="min-width:100%;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top:9px;padding-right:9px;padding-left:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                            <tbody><tr>
                                <td align="center" valign="top" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                    <tr>
                                    <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                <tbody><tr>
                                                    <td valign="top"  class="mcnFollowContentItemContainer" style="padding-right:10px;padding-bottom:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                                    <a href="https://www.facebook.com/AgaveLab" target="_blank" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" ><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-facebook-48.png"  height="24" width="24" class="" style="display:block;-ms-interpolation-mode:bicubic;border-width:0;height:auto;outline-style:none;text-decoration:none;" ></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                <tbody><tr>
                                                    <td valign="top"  class="mcnFollowContentItemContainer" style="padding-right:10px;padding-bottom:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                                    <a href="https://www.instagram.com/agavelab/" target="_blank" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" ><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-instagram-48.png"  height="24" width="24" class="" style="display:block;-ms-interpolation-mode:bicubic;border-width:0;height:auto;outline-style:none;text-decoration:none;" ></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                <tbody><tr>
                                                    <td valign="top"  class="mcnFollowContentItemContainer" style="padding-right:10px;padding-bottom:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                                    <a href="quiz.agavelab.com" target="_blank" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" ><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-link-48.png"  height="24" width="24" class="" style="display:block;-ms-interpolation-mode:bicubic;border-width:0;height:auto;outline-style:none;text-decoration:none;" ></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                <tbody><tr>
                                                    <td valign="top"  class="mcnFollowContentItemContainer" style="padding-right:0;padding-bottom:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px;mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                        <tbody>
                                                                        <tr>
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
                                                                                    <a href="mailto:training@agavelab.com" target="_blank" style="mso-line-height-rule:exactly;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" ><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-forwardtofriend-48.png"  height="24" width="24" class="" style="display:block;-ms-interpolation-mode:bicubic;border-width:0;height:auto;outline-style:none;text-decoration:none;" ></a>
                                                                                </td> 
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table></td>
                                                  </tr>
                                             </table>
                                             <!--[if (gte mso 9)|(IE)]>
                                             </td>
                                             </tr>
                                             </table>
                                             <![endif]-->
                                        </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html> `;

    const msg = {
        to: user.email,
        from: 'training@agavelab.com',
        subject: 'Dev Factory',
        html: htmlMail
    };

    const msgCopy = {
        to: 'bobanbrban@gmail.com',
        from: 'training@agavelab.com',
        subject: 'Dev Factory',
        html: htmlMail
    };

    let sendTwoMails = (msg, msgCopy) => {
        sgMail.send(msg).then(data => console.log("msg has been sent to the user", data)).catch(err => console.error(err));
        sgMail.send(msgCopy).then(data => console.log("msgCopy has been sent to the user", data)).catch(err => console.error(err));
    };
    return sendTwoMails(msg, msgCopy);
});
